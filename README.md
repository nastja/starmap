StaRMAP – A NAStJA Application
==================
StaRMAP (**Sta**ggered grid **R**adiation **M**oment **Ap**proximation) is a simple method to solve spherical harmonics moment systems, such as the time-dependent P<sub>N</sub> equations, of radiative transfer.

We port the original Matlab [StaRMAP code](https://www.math.temple.edu/~seibold/research/starmap/) by B. Seibold and M. Frank to C++ and integrate it into the [NAStJA framework](https://gitlab.com/nastja/nastja). The parallelism provided by the NAStJA framework allows efficient, fast, three-dimensional simulations of large domains.

# Prerequisits
- CMake 3.10+
- C++14 compliant compilter:
  - Clang 6, 7, 8, 9, 10
  - GCC 7, 8, 9
- Message Passing Interface (MPI) library

Running tests and building the documentation has the following additional requirements:
- doxygen
- jq

# Quick Build
To build NAStJA, use the standard CMake procedure
```
mkdir build
cd build
cmake ..
make
```

Examples can be found in the `test/data`, e.g.,
```
./nastja_starmap -c ../test/data/starmap_v00 -o output
```

For more compiler options see the [NAStJA repository](https://gitlab.com/nastja/nastja).

# Please cite us
If you use NAStJA implementation of StaRMAP in a publication, please cite:
> M. Berghoff, M. Frank, and B. Seibold. *StaRMAP – A NAStJA Application*. Version 1.0. 2020. doi:[10.5281/zenodo.3741415](https://dx.doi.org/10.5281/zenodo.3741415).

# Related Publications
* M. Berghoff, J. Rosenbauer, and N. Pfisterer. The NAStJA Framework. Version 1.0. 2020. doi:[10.5281/zenodo.3740079](https://doi.org/10.5281/zenodo.3740079).
* M. Berghoff, I. Kondov, and J. Hötzer. Massively parallel stencil code solver with autonomous adaptive block distribution. In: IEEE Transactions on Parallel and Distributed Systems, 2018. doi:[10.1109/TPDS.2018.2819672](https://doi.org/10.1109/TPDS.2018.2819672).
* B. Seibold and M. Frank. StaRMAP - A second order staggered grid method for spherical harmonics moment equations of radiative transfer. ACM Transactions on Mathematical Software, Vol. 41, No. 1, 2014. doi:[10.1145/2590808](https://doi.org/10.1145/2590808).
* E. Olbrant, E. W. Larsen, M. Frank, and B. Seibold. Asymptotic derivation and numerical investigation of time-dependent simplified PN equations. Journal of Computational Physics, Vol. 238, 2013, pp. 315-336. doi:[10.1016/j.jcp.2012.10.055](https://doi.org/10.1016/j.jcp.2012.10.055).

------------------
Mozilla Public License, v. 2.0 (MPL-2.0)

Copyright 2020 Marco Berghoff
