stages:
  - build
  - test
  - deployment

.template: &variables
  NASTJA_ENABLE_AVX: "ON"
  NASTJA_COVERAGE: "OFF"
  CMAKE_BUILD_TYPE: "Release"
  CXX: /usr/bin/clang++-9

.template: &script_build
  - mkdir -p /builds/nastja/starmap/${CI_JOB_NAME}
  - cd /builds/nastja/starmap/${CI_JOB_NAME}
  - cmake ..
      -DNASTJA_COVERAGE=${NASTJA_COVERAGE}
      -DNASTJA_ENABLE_AVX=${NASTJA_ENABLE_AVX}
      -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
      -G "Ninja"
  - ninja
  - /builds/nastja/starmap/scripts/compress.sh

.template: &script_test
  - cd /builds/nastja/starmap/${CI_JOB_NAME/test/build}
  - tar xf artifacts.tar.gz
  - ctest -T test --output-on-failure

.template: &script_after_test
  - cd /builds/nastja/starmap/${CI_JOB_NAME/test/build}
  - xsltproc /builds/nastja/starmap/external/nastja/scripts/ci/ctest2junit.xsl Testing/$(head -n 1 < Testing/TAG)/Test.xml > junit_report_${CI_JOB_NAME}.xml
  - /builds/nastja/starmap/external/nastja/scripts/ci/junit_expand.sh junit_report_${CI_JOB_NAME}.xml

.template: &build
  stage: build
  image: nastja/nastja-build:clang
  tags:
    - docker
  variables: *variables
  script: *script_build
  artifacts:
    paths:
    - /builds/nastja/starmap/${CI_JOB_NAME}/artifacts.tar.gz
    expire_in: 1h
  before_script:
    - export CCACHE_BASEDIR=${PWD}
    - export CCACHE_DIR=/ccache

.template: &build_gcc
  stage: build
  image: nastja/nastja-build:gcc
  tags:
    - docker
  variables: *variables
  script: *script_build
  artifacts:
    paths:
    - /builds/nastja/starmap/${CI_JOB_NAME}/artifacts.tar.gz
    expire_in: 1h
  before_script:
    - export CCACHE_BASEDIR=${PWD}
    - export CCACHE_DIR=/ccache

.template: &test
  image: nastja/nastja-build:clang
  stage: test
  tags:
    - docker
  script: *script_test
  after_script: *script_after_test
  artifacts:
    reports:
      junit: /builds/nastja/starmap/${CI_JOB_NAME/test/build}/junit_report_${CI_JOB_NAME}.xml

.template: &test_gcc
  image: nastja/nastja-build:gcc
  stage: test
  tags:
    - docker
  script: *script_test
  after_script: *script_after_test
  artifacts:
    reports:
      junit: /builds/nastja/starmap/${CI_JOB_NAME/test/build}/junit_report.xml

build_clang:
  <<: *build

build_clang_debug:
  <<: *build
  variables:
    <<: *variables
    CMAKE_BUILD_TYPE: "Debug"
    NASTJA_COVERAGE: "ON"

build_gcc_8:
  <<: *build_gcc
  variables:
    <<: *variables
    CXX: /usr/bin/g++-8
  only:
    - schedules


test_clang:
  <<: *test
  needs: ["build_clang"]

test_clang_debug:
  <<: *test
  script:
    - *script_test
    - /builds/nastja/starmap/scripts/llvm_coverage.sh
  needs: ["build_clang_debug"]
  artifacts:
    paths:
    - /builds/nastja/starmap/${CI_JOB_NAME/test/build}/coverage.profdata
    - /builds/nastja/starmap/${CI_JOB_NAME/test/build}/nastja_starmap
    - /builds/nastja/starmap/${CI_JOB_NAME/test/build}/unittest/ut_starmap
    expire_in: 1h
    reports:
      junit: /builds/nastja/starmap/${CI_JOB_NAME/test/build}/junit_report_${CI_JOB_NAME}.xml

.test_gcc_8:
  <<: *test_gcc
  needs: ["build_gcc_8"]
  only:
    - schedules

deploy_clang_debug:
  image: nastja/nastja-build:clang
  only:
    - master
  stage: deployment
  needs: ["test_clang_debug"]
  script:
    - cd /builds/nastja/starmap/${CI_JOB_NAME/deploy/build}
    - cmake .. -G "Ninja"
    - /builds/nastja/starmap/scripts/llvm_covreport.sh
    - if [ "$CI_COMMIT_REF_SLUG" == "master" ]; then
    - ninja doc
    - fi
  artifacts:
    paths:
    - /builds/nastja/starmap/${CI_JOB_NAME/deploy/build}/coverage
    - /builds/nastja/starmap/${CI_JOB_NAME/deploy/build}/docs/html

pages:
  image: nastja/nastja-build:clang
  only:
    - master
  script:
    - if [ "$CI_COMMIT_REF_SLUG" == "master" ]; then
    -   mkdir public
    -   branch=$(echo $CI_COMMIT_REF_SLUG | cut -d"-" -f1)
    -   mv /builds/nastja/starmap/build_clang_debug/coverage public/coverage
    -   if [ -d /builds/nastja/starmap/build_clang_debug/docs/html ]; then mv /builds/nastja/starmap/build_clang_debug/docs/html public/docs; fi
    - fi
  stage: .post
  artifacts:
    paths:
    - public
  needs: ["deploy_clang_debug"]
