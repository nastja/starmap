/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "helper/starmapmath.h"

using namespace starmap::math;

TEST_CASE("Coordinates", "[unit][math]") {
  SECTION("coord") {
    CHECK(coord(16, 1, 1) == 0);
    CHECK(coord0(16, 1, 1) == 17);
  }
}
