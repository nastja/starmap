/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_starmap3dv2.h"
#include "helper/datastarmap.h"
#include "helper/starmapmath.h"
#include "fmt/core.h"
#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <numeric>
#include <set>
#include <vector>

namespace starmap {

using namespace math;
using namespace nastja;
using namespace nastja::stencil;

void SweepStaRMAP3Dv2::init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) {
  Sweep::init(block, fieldpool);
}

void SweepStaRMAP3Dv2::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }
  auto& data = getSimData().initData<DataStaRMAP>();
  data.configure(getSimData().getConfig());
}

void SweepStaRMAP3Dv2::executeBlock(block::Block* block) {
  auto& fieldU    = block->getFieldByName("u")->get<real_t>();
  auto& fieldData = block->getFieldByName("data")->get<real_t>();
  auto& data      = getSimData().getData<DataStaRMAP>();

  std::array<std::array<real_t, 8>, 5> interpolated{};

  auto view = fieldU.createView();
  for (auto cell = view.begin(); cell != view.end(); ++cell) {
    auto x = cell.coordinates().x();
    auto y = cell.coordinates().y();
    auto z = cell.coordinates().z();
    __asm volatile("# LLVM-MCA-BEGIN starmap3d_1");
    auto indexU = fieldU.getIndex(x, y, z);
    auto index  = fieldData.getIndex(x, y, z);
    getInterpolated(interpolated, index, fieldData, 3);
    // useMaingrid(interpolated, index, fieldData, 3);
    // useSubgrids(interpolated, index, fieldData, 3);

    calcDifferencesEven_ordered(indexU, fieldU, data, getSimData().deltax_R);

    for (auto j : data.c211) {
      real_t sumW = calcSumW(j, data);
      real_t q    = 0.0;
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid211] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid211];
    }
    for (auto j : data.c121) {
      real_t sumW = calcSumW(j, data);
      real_t q    = 0.0;
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid121] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid121];
    }
    for (auto j : data.c112) {
      real_t sumW = calcSumW(j, data);
      real_t q    = 0.0;
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid112] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid112];
    }
    for (auto j : data.c222) {
      real_t sumW = calcSumW(j, data);
      real_t q    = 0.0;
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid222] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid222];
    }

    __asm volatile("# LLVM-MCA-END starmap3d_1");
  }
}

void SweepStaRMAP3Dv2::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

void SweepStaRMAP3Dv2::setup(const block::BlockRegister& /*blockRegister*/) {
  if (isSetuped) return;
  isSetuped  = true;
  auto& data = getSimData().getData<DataStaRMAP>();
  setupClosure(data);
  makegrid(data);
  reorderIndizes(data);
}

}  // namespace starmap
