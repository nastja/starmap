/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include <array>
#include <vector>

namespace starmap {

/**
 * Class for sweep StaRMAP.
 *
 * @ingroup sweeps
 */
class SweepStaRMAP3D2v9 : public nastja::Sweep {
public:
  explicit SweepStaRMAP3D2v9() {
    registerFieldname("u");
    registerFieldname("data");
  }

  ~SweepStaRMAP3D2v9() override                   = default;
  SweepStaRMAP3D2v9(const SweepStaRMAP3D2v9&)     = delete;
  SweepStaRMAP3D2v9(SweepStaRMAP3D2v9&&) noexcept = default;
  SweepStaRMAP3D2v9& operator=(const SweepStaRMAP3D2v9&) = delete;
  SweepStaRMAP3D2v9& operator=(SweepStaRMAP3D2v9&&) = delete;

  void executeBlock(nastja::block::Block* block) override;
  void execute(const nastja::block::BlockRegister& blockRegister) override;
};

}  // namespace starmap
