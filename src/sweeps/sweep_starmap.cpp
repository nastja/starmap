/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_starmap.h"
#include "helper/starmapmath.h"
#include "fmt/core.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <set>
#include <vector>

namespace starmap {

using namespace math;
using namespace nastja;
using namespace nastja::stencil;

void SweepStaRMAP::init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) {
  Sweep::init(block, fieldpool);
  // block->registerDataContainer(getName(), new CellBlockContainer(block));
}

void SweepStaRMAP::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }
  getSimData().initData<DataStaRMAP>();

  /// @key{StaRMAP.closure, string, "P"}
  /// @values{"P", "SP"}
  /// Type of closure.
  auto closure = getSimData().getConfig().getValue<std::string>("StaRMAP.closure"_jptr, "P");
  if (closure == "P") {
    closure_ = Closure::P;
  } else if (closure == "SP") {
    closure_ = Closure::SP;
  } else {
    throw(std::invalid_argument("'StarMAP.closure must be 'P' or 'SP'."));
  }

  /// @key{StaRMAP.n_mom, uint}
  /// Order of moment approximation.
  n_mom = getSimData().getConfig().getValue<unsigned int>("StaRMAP.n_mom"_jptr);
}

void SweepStaRMAP::executeBlock(block::Block* block) {
  auto& fieldU    = block->getFieldByName("u")->get<real_t>();
  auto& fieldData = block->getFieldByName("data")->get<real_t>();
  auto& data      = getSimData().getData<DataStaRMAP>();

  const auto& boundarySize = fieldU.getBoundarySize();

  std::vector<real_t> dxU(data.n_sys);
  std::vector<real_t> dyU(data.n_sys);

  for (unsigned long z = boundarySize[2]; z < fieldU.getSize(2) - boundarySize[2]; z++) {
    for (unsigned long y = boundarySize[1]; y < fieldU.getSize(1) - boundarySize[1]; y++) {
      for (unsigned long x = boundarySize[0]; x < fieldU.getSize(0) - boundarySize[0]; x++) {
        auto indexU = fieldU.getIndex(x, y, z);
        auto index  = fieldData.getIndex(x, y, z);

        // case 1                    % Half step with (2,1) and (1,2) grids.
        // for j = c11       % Compute update at (2,1) and (1,2) from (1,1).
        //     dxU{j} = diff(U{j}(extendx{1},:),[],1)/h(1);
        //     dyU{j} = diff(U{j}(:,extendy{1}),[],2)/h(2);
        // end
        for (auto j : data.c11) {
          dxU[j] = (fieldU.getCell(indexU, Direction::R, j) - fieldU.getCell(indexU, j)) * getSimData().deltax_R;
          dyU[j] = (fieldU.getCell(indexU, Direction::U, j) - fieldU.getCell(indexU, j)) * getSimData().deltax_R;
        }
        // for j = c22       % Compute update at (1,2) and (2,1) from (2,2).
        //     dxU{j} = diff(U{j}(extendx{2},:),[],1)/h(1);
        //     dyU{j} = diff(U{j}(:,extendy{2}),[],2)/h(2);
        // end
        for (auto j : data.c22) {
          dxU[j] = (fieldU.getCell(indexU, j) - fieldU.getCell(indexU, Direction::L, j)) * getSimData().deltax_R;
          dyU[j] = (fieldU.getCell(indexU, j) - fieldU.getCell(indexU, Direction::D, j)) * getSimData().deltax_R;
        }

        // for j = [c21 c12]   % Update components on (2,1) and (1,2) grids.
        //     W = -sumcell([dxU(Ix{j}),dyU(Iy{j})],[par.Mx(j,Ix{j}),par.My(j,Iy{j})]);
        //     U{j} = U{j}+dt/2*(W-U{j})*0.999;
        //     U{j} = U{j}+dt/2*(W+Q{j}-sT1{gtx(j),gty(j),par.mom_order(j)}.*U{j}).*ET{gtx(j),gty(j),par.mom_order(j)};
        // end
        for (auto j : data.c21) {
          real_t sumW = 0.0;
          for (unsigned int i = 0; i < MatrixWidth; i++) {
            sumW -= dxU[data.Ix[MatrixWidth * j + i]] * data.Mx[MatrixWidth * j + i];
            sumW -= dyU[data.Iy[MatrixWidth * j + i]] * data.My[MatrixWidth * j + i];
          }
          real_t q = 0.0;
          if (j == 0) q = fieldData.getCell(index, 0);
          fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - fieldData.getCell(index, 2) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 4);
        }
        for (auto j : data.c12) {
          real_t sumW = 0.0;
          for (unsigned int i = 0; i < MatrixWidth; i++) {
            sumW -= dxU[data.Ix[MatrixWidth * j + i]] * data.Mx[MatrixWidth * j + i];
            sumW -= dyU[data.Iy[MatrixWidth * j + i]] * data.My[MatrixWidth * j + i];
          }
          real_t q = 0.0;
          if (j == 0) q = fieldData.getCell(index, 0);
          fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - fieldData.getCell(index, 2) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 4);
        }
      }
    }
  }
}

void SweepStaRMAP::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

void SweepStaRMAP::setup(const block::BlockRegister& /*blockRegister*/) {
  if (isSetuped) return;
  isSetuped = true;
  if (closure_ == Closure::P) {
    setupClosureP();
  } else if (closure_ == Closure::SP) {
    setupClosureSP();
  } else {
    throw(std::invalid_argument("Unknown closure."));
  }
  makegrid();
  compressMatrix();
}

void SweepStaRMAP::compressMatrix() {
  auto& data = getSimData().getData<DataStaRMAP>();
  data.Mx.resize(MatrixWidth * data.n_sys);
  data.My.resize(MatrixWidth * data.n_sys);

  for (unsigned int j = 0; j < data.n_sys; j++) {
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (data.Ix[MatrixWidth * j + i] != -1) {
        data.Mx[MatrixWidth * j + i] = Mx[coord0(data.n_sys, j, data.Ix[MatrixWidth * j + i])];
      } else {
        data.Ix[MatrixWidth * j + i] = i == 0 ? 0 : data.Ix[MatrixWidth * j + i - 1];
      }
      if (data.Iy[MatrixWidth * j + i] != -1) {
        data.My[MatrixWidth * j + i] = My[coord0(data.n_sys, j, data.Iy[MatrixWidth * j + i])];
      } else {
        data.Iy[MatrixWidth * j + i] = i == 0 ? 0 : data.Iy[MatrixWidth * j + i - 1];
      }
    }
  }
}

void SweepStaRMAP::makegrid() {
  auto& data = getSimData().getData<DataStaRMAP>();
  data.Ix.resize(MatrixWidth * data.n_sys);
  data.Iy.resize(MatrixWidth * data.n_sys);
  std::fill(data.Ix.begin(), data.Ix.end(), -1);
  std::fill(data.Iy.begin(), data.Iy.end(), -1);

  // data.Ix, data.Iy
  for (unsigned int j = 0; j < data.n_sys; j++) {
    int cntx = 0;
    int cnty = 0;
    for (unsigned int i = 0; i < data.n_sys; i++) {
      if (Mx[coord0(data.n_sys, j, i)] != 0.0) {
        data.Ix[MatrixWidth * j + cntx] = i;
        cntx++;
      }
      if (My[coord0(data.n_sys, j, i)] != 0.0) {
        data.Iy[MatrixWidth * j + cnty] = i;
        cnty++;
      }
    }
  }

  for (unsigned int j = 0; j < data.n_sys; j++) {
    std::cout << fmt::format("[{},{},{}] ", data.Ix[MatrixWidth * j + 0], data.Ix[MatrixWidth * j + 1], data.Ix[MatrixWidth * j + 2]);
  }
  std::cout << "\n";

  for (unsigned int j = 0; j < data.n_sys; j++) {
    std::cout << fmt::format("[{},{},{}] ", data.Iy[MatrixWidth * j + 0], data.Iy[MatrixWidth * j + 1], data.Iy[MatrixWidth * j + 2]);
  }
  std::cout << "\n";

  // c11, c22, c21, c12
  std::set<int> cs11;
  std::set<int> cs12;
  std::set<int> cs21;
  std::set<int> cs22;
  cs11.insert(0);
  while (cs11.size() + cs12.size() + cs21.size() + cs22.size() < data.n_sys) {
    addIndices(cs21, data.Ix, cs11);
    addIndices(cs21, data.Iy, cs22);
    addIndices(cs12, data.Iy, cs11);
    addIndices(cs12, data.Ix, cs22);
    addIndices(cs11, data.Ix, cs21);
    addIndices(cs11, data.Iy, cs12);
    addIndices(cs22, data.Iy, cs21);
    addIndices(cs22, data.Ix, cs12);
  }

  data.gtx.resize(data.n_sys);
  data.gty.resize(data.n_sys);
  data.mom_order.resize(data.n_sys);

  std::fill_n(data.gtx.begin(), data.n_sys, 1);
  std::fill_n(data.gty.begin(), data.n_sys, 1);

  std::fill_n(data.mom_order.begin(), data.n_sys, 1);
  data.mom_order[0] = 0;

  std::cout << "c11: ";
  for (const auto& j : cs11) {
    data.c11.push_back(j);
    std::cout << j << " ";
  }

  std::cout << "\nc12: ";
  for (const auto& j : cs12) {
    data.c12.push_back(j);
    data.gty[j] = 2;
    std::cout << j << " ";
  }

  std::cout << "\nc21: ";
  for (const auto& j : cs21) {
    data.gtx[j] = 2;
    data.c21.push_back(j);
    std::cout << j << " ";
  }

  std::cout << "\nc22: ";
  for (const auto& j : cs22) {
    data.gtx[j] = 2;
    data.gty[j] = 2;
    data.c22.push_back(j);
    std::cout << j << " ";
  }
  std::cout << "\n";
}

void SweepStaRMAP::setupClosureP() {
  auto& data = getSimData().getData<DataStaRMAP>();
  data.n_sys = (n_mom + 1) * (n_mom + 2) / 2;  // number of system components

  Mx.resize(data.n_sys * data.n_sys);
  My.resize(data.n_sys * data.n_sys);

  for (int m = 1; m <= n_mom; m++) {
    for (int i = 1; i <= m; i++) {
      int p    = m * (m - 1) / 2 + i;
      real_t v = D(m, -m + 2 * (std::ceil(i / 2) - 1));

      Mx[coord(data.n_sys, p, p + m)]                         = v;
      My[coord(data.n_sys, p, p + m - (i % 2 == 0 ? 1 : -1))] = -(i % 2 == 0 ? v : -v);
    }
    for (int i = 1; i <= m - 1; i++) {
      int p    = m * (m - 1) / 2 + i;
      real_t v = F(m, -m + 2 + 2 * (std::ceil(i / 2) - 1));

      Mx[coord(data.n_sys, p, p + m + 2)]                         = -v;
      My[coord(data.n_sys, p - (i % 2 == 0 ? 1 : -1), p + m + 2)] = (i % 2 == 0 ? v : -v);
    }
  }

  for (int m = 1; m <= n_mom; m += 2) {
    int i = m * (m + 1) / 2;
    for (unsigned int j = 0; j < data.n_sys; j++) {
      Mx[coord(data.n_sys, i, 1) + j] *= M_SQRT2;
      My[coord(data.n_sys, i, 1) + j] *= M_SQRT2;
    }
  }

  for (int m = 2; m <= n_mom; m += 2) {
    int i = (m + 1) * (m + 2) / 2;
    for (unsigned int j = 0; j < data.n_sys; j++) {
      Mx[coord(data.n_sys, 1 + j, i)] *= M_SQRT2;
      My[coord(data.n_sys, 1 + j, i)] *= M_SQRT2;
    }
  }

  for (unsigned int i = 1; i <= data.n_sys; i++) {
    for (unsigned int j = i; j <= data.n_sys; j++) {
      real_t val = 0.5 * (Mx[coord(data.n_sys, i, j)] + Mx[coord(data.n_sys, j, i)]);

      Mx[coord(data.n_sys, i, j)] = val;
      Mx[coord(data.n_sys, j, i)] = val;

      val = 0.5 * (My[coord(data.n_sys, i, j)] + My[coord(data.n_sys, j, i)]);

      My[coord(data.n_sys, i, j)] = val;
      My[coord(data.n_sys, j, i)] = val;
    }
  }
}

void SweepStaRMAP::setupClosureSP() {
  auto& data         = getSimData().getData<DataStaRMAP>();
  unsigned int nstar = std::ceil((n_mom + 1) / 2);
  data.n_sys         = nstar * 3;
  Mx.resize(data.n_sys * data.n_sys);
  My.resize(data.n_sys * data.n_sys);
  for (unsigned int i = 0; i < nstar; i++) {
    real_t k = 2. * i * (2. * i - 1.) / ((4. * i + 1.) * (4. * i - 1.));
    real_t l = 0;
    if (i < nstar - 1 || std::floor(n_mom / 2) * 2 < n_mom) {
      l = 4. * i * i / ((4. * i + 1.) * (4. * i - 1.)) + (2. * i + 1.) * (2. * i + 1.) / ((4. * i + 1.) * (4. * i + 3.));
    }
    real_t m = 2. * (2. * i + 1.) * (i + 1.) / ((4. * i + 1.) * (4. * i + 3.));

    Mx[coord0(data.n_sys, nstar + i * 2, i)] = l;
    if (i != 0) Mx[coord0(data.n_sys, nstar + i * 2, i - 1)] = k;
    if (i != nstar - 1) Mx[coord0(data.n_sys, nstar + i * 2, i + 1)] = m;
    Mx[coord0(data.n_sys, i, nstar + i * 2)] = 1.0;
  }
  for (unsigned int i = 0; i < data.n_sys; i++) {
    for (unsigned int j = 0; j < data.n_sys; j++) {
      int i2 = i;
      int j2 = j;
      if (i >= nstar) i2 += i % 2 == 0 ? 1 : -1;  // TODO nstar = even?
      if (j >= nstar) j2 += j % 2 == 0 ? 1 : -1;
      My[coord0(data.n_sys, i, j)] = Mx[coord0(data.n_sys, i2, j2)];
    }
  }

  // for (int i = 0; i < data.n_sys; i++) {
  //   for (unsigned int j = 0; j < data.n_sys; j++) {
  //     std::cout << fmt::format("{:6.4f} ", My .at(coord0(data.n_sys, i, j)));
  //   }
  //   std::cout << "\n";
  // }
}

}  // namespace starmap
