/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include <array>
#include <vector>

namespace starmap {

/**
 * Class for sweep StaRMAP.
 *
 * @ingroup sweeps
 */
class SweepStaRMAP3D2v5 : public nastja::Sweep {
public:
  explicit SweepStaRMAP3D2v5() {
    registerFieldname("u");
    registerFieldname("data");
  }

  ~SweepStaRMAP3D2v5() override                   = default;
  SweepStaRMAP3D2v5(const SweepStaRMAP3D2v5&)     = delete;
  SweepStaRMAP3D2v5(SweepStaRMAP3D2v5&&) noexcept = default;
  SweepStaRMAP3D2v5& operator=(const SweepStaRMAP3D2v5&) = delete;
  SweepStaRMAP3D2v5& operator=(SweepStaRMAP3D2v5&&) = delete;

  void executeBlock(nastja::block::Block* block) override;
  void execute(const nastja::block::BlockRegister& blockRegister) override;
};

}  // namespace starmap
