/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include "lib/field/memory.h"
#include <array>
#include <vector>

namespace starmap {

/**
 * Class for sweep StaRMAP.
 *
 * @ingroup sweeps
 */
class SweepStaRMAP3Dv9 : public nastja::Sweep {
public:
  explicit SweepStaRMAP3Dv9() {
    registerFieldname("u");
    registerFieldname("data");
  }

  ~SweepStaRMAP3Dv9() override                  = default;  //TODO dealloc interpolated, dU
  SweepStaRMAP3Dv9(const SweepStaRMAP3Dv9&)     = delete;
  SweepStaRMAP3Dv9(SweepStaRMAP3Dv9&&) noexcept = default;
  SweepStaRMAP3Dv9& operator=(const SweepStaRMAP3Dv9&) = delete;
  SweepStaRMAP3Dv9& operator=(SweepStaRMAP3Dv9&&) = delete;

  void init(nastja::block::Block* block, const nastja::Pool<nastja::field::FieldProperties>& fieldpool) override;
  void init(const nastja::block::BlockRegister& blockRegister, const nastja::Pool<nastja::field::FieldProperties>& fieldpool) override;
  void executeBlock(nastja::block::Block* block) override;
  void execute(const nastja::block::BlockRegister& blockRegister) override;
  void setup(const nastja::block::BlockRegister& blockRegister) override;

private:
  bool isSetuped{false};
};

}  // namespace starmap
