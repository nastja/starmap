/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include <array>
#include <vector>

namespace starmap {

/**
 * Class for sweep StaRMAP.
 *
 * @ingroup sweeps
 */
class SweepStaRMAP : public nastja::Sweep {
public:
  explicit SweepStaRMAP() {
    registerFieldname("u");
    registerFieldname("data");
  }

  ~SweepStaRMAP() override              = default;
  SweepStaRMAP(const SweepStaRMAP&)     = delete;
  SweepStaRMAP(SweepStaRMAP&&) noexcept = default;
  SweepStaRMAP& operator=(const SweepStaRMAP&) = delete;
  SweepStaRMAP& operator=(SweepStaRMAP&&) = delete;

  void init(nastja::block::Block* block, const nastja::Pool<nastja::field::FieldProperties>& fieldpool) override;
  void init(const nastja::block::BlockRegister& blockRegister, const nastja::Pool<nastja::field::FieldProperties>& fieldpool) override;
  void executeBlock(nastja::block::Block* block) override;
  void execute(const nastja::block::BlockRegister& blockRegister) override;
  void setup(const nastja::block::BlockRegister& blockRegister) override;

private:
  enum class Closure { P,
                       SP };

  void setupClosureP();
  void setupClosureSP();
  void makegrid();
  void compressMatrix();

  Closure closure_;  ///< Type of closure.
  int n_mom;         ///< Order of moment approximation.
  bool isSetuped{false};

  std::vector<nastja::real_t> Mx;
  std::vector<nastja::real_t> My;
};

}  // namespace starmap
