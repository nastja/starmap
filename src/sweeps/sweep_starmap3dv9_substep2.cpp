/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#include "sweep_starmap3dv9_substep2.h"
#include "helper/starmapmath.h"
#include "fmt/core.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <set>
#include <vector>

namespace starmap {

using namespace math;
using namespace nastja;
using namespace nastja::simd;
using namespace nastja::stencil;

void SweepStaRMAP3D2v9::executeBlock(block::Block* block) {
  auto& fieldU    = block->getFieldByName("u")->get<real_t, true>();
  auto& fieldData = block->getFieldByName("data")->get<real_t, true>();
  auto& data      = getSimData().getData<DataStaRMAP>();

  const float64x4_t deltax_R = set_pd(getSimData().deltax_R);
  const float64x4_t deltath  = set_pd(0.5 * getSimData().deltat);

  auto* dU           = data.dUavx;
  auto* interpolated = data.interpolated;

  auto view = fieldU.createView<4>();
  for (const auto& cell : view) {
    __asm volatile("# LLVM-MCA-BEGIN starmap3d_2");

    auto index = view.getIndex(cell) / data.n_sys * 5;

    getInterpolated4(interpolated, index, fieldData);
    // useMaingrid(interpolated, index, fieldData);
    // useSubgrids(interpolated, index, fieldData);

    for (int j = data.latticeEnd[4]; j < data.latticeEnd[5]; j++) {
      auto dxU = (view.load(cell, j) - view.load_unaligned(cell, Direction::L, j)) * deltax_R;
      auto dyU = (view.load(cell, Direction::U, j) - view.load(cell, j)) * deltax_R;
      auto dzU = (view.load(cell, Direction::F, j) - view.load(cell, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }
    for (int j = data.latticeEnd[5]; j < data.latticeEnd[6]; j++) {
      auto dxU = (view.load_unaligned(cell, Direction::R, j) - view.load(cell, j)) * deltax_R;
      auto dyU = (view.load(cell, j) - view.load(cell, Direction::D, j)) * deltax_R;
      auto dzU = (view.load(cell, Direction::F, j) - view.load(cell, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }
    for (int j = data.latticeEnd[6]; j < data.latticeEnd[7]; j++) {
      auto dxU = (view.load_unaligned(cell, Direction::R, j) - view.load(cell, j)) * deltax_R;
      auto dyU = (view.load(cell, Direction::U, j) - view.load(cell, j)) * deltax_R;
      auto dzU = (view.load(cell, j) - view.load(cell, Direction::B, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }
    for (int j = data.latticeEnd[7]; j < data.latticeEnd[8]; j++) {
      auto dxU = (view.load(cell, j) - view.load_unaligned(cell, Direction::L, j)) * deltax_R;
      auto dyU = (view.load(cell, j) - view.load(cell, Direction::D, j)) * deltax_R;
      auto dzU = (view.load(cell, j) - view.load(cell, Direction::B, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }

    auto q  = load(interpolated + 4 * (8 * data_q + grid111));
    auto sA = load(interpolated + 4 * (8 * data_sa + grid111));
    auto eA = load(interpolated + 4 * (8 * data_ea + grid111));
    auto sT = load(interpolated + 4 * (8 * data_st + grid111));
    auto eT = load(interpolated + 4 * (8 * data_et + grid111));
    for (int j = data.latticeEnd[0]; j < data.latticeEnd[1]; j++) {
      auto sumW = calcSumW_avx(j, data);

      auto Uj = view.load(cell, j);
      if (j == 0) {
        Uj += deltath * (sumW + q - sA * Uj) * eA;
        Uj += deltath * (sumW + q - sA * Uj) * eA;
      } else {
        Uj += deltath * (sumW - sT * Uj) * eT;
        Uj += deltath * (sumW - sT * Uj) * eT;
      }
      view.store(cell, Uj, j);
    }

    sT = load(interpolated + 4 * (8 * data_st + grid221));
    eT = load(interpolated + 4 * (8 * data_et + grid221));
    for (int j = data.latticeEnd[1]; j < data.latticeEnd[2]; j++) {
      auto sumW = calcSumW_avx(j, data);

      auto Uj = view.load(cell, j);
      Uj += deltath * (sumW - sT * Uj) * eT;
      Uj += deltath * (sumW - sT * Uj) * eT;
      view.store(cell, Uj, j);
    }

    sT = load(interpolated + 4 * (8 * data_st + grid212));
    eT = load(interpolated + 4 * (8 * data_et + grid212));
    for (int j = data.latticeEnd[2]; j < data.latticeEnd[3]; j++) {
      auto sumW = calcSumW_avx(j, data);

      auto Uj = view.load(cell, j);
      Uj += deltath * (sumW - sT * Uj) * eT;
      Uj += deltath * (sumW - sT * Uj) * eT;
      view.store(cell, Uj, j);
    }

    sT = load(interpolated + 4 * (8 * data_st + grid122));
    eT = load(interpolated + 4 * (8 * data_et + grid122));
    for (int j = data.latticeEnd[3]; j < data.latticeEnd[4]; j++) {
      auto sumW = calcSumW_avx(j, data);

      auto Uj = view.load(cell, j);
      Uj += deltath * (sumW - sT * Uj) * eT;
      Uj += deltath * (sumW - sT * Uj) * eT;
      view.store(cell, Uj, j);
    }

    __asm volatile("# LLVM-MCA-END starmap3d_2");
  }
}

void SweepStaRMAP3D2v9::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace starmap
