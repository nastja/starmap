/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include <array>
#include <vector>

namespace starmap {

/**
 * Class for sweep StaRMAP.
 *
 * @ingroup sweeps
 */
class SweepStaRMAP2 : public nastja::Sweep {
public:
  explicit SweepStaRMAP2() {
    registerFieldname("u");
    registerFieldname("data");
  }

  ~SweepStaRMAP2() override               = default;
  SweepStaRMAP2(const SweepStaRMAP2&)     = delete;
  SweepStaRMAP2(SweepStaRMAP2&&) noexcept = default;
  SweepStaRMAP2& operator=(const SweepStaRMAP2&) = delete;
  SweepStaRMAP2& operator=(SweepStaRMAP2&&) = delete;

  void executeBlock(nastja::block::Block* block) override;
  void execute(const nastja::block::BlockRegister& blockRegister) override;
};

}  // namespace starmap
