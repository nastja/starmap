/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_starmap3d_substep2.h"
#include "helper/starmapmath.h"
#include "fmt/core.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <set>
#include <vector>

namespace starmap {

using namespace math;
using namespace nastja;
using namespace nastja::stencil;

void SweepStaRMAP3D2::executeBlock(block::Block* block) {
  auto& fieldU    = block->getFieldByName("u")->get<real_t>();
  auto& fieldData = block->getFieldByName("data")->get<real_t>();
  auto& data      = getSimData().getData<DataStaRMAP>();

  std::array<std::array<real_t, 8>, 5> interpolated{};

  auto view = fieldU.createView();
  for (auto cell = view.begin(); cell != view.end(); ++cell) {
    auto x = cell.coordinates().x();
    auto y = cell.coordinates().y();
    auto z = cell.coordinates().z();
    __asm volatile("# LLVM-MCA-BEGIN starmap3d_2");

    auto indexU = fieldU.getIndex(x, y, z);
    auto index  = fieldData.getIndex(x, y, z);

    getInterpolated(interpolated, index, fieldData);
    // useMaingrid(interpolated, index, fieldData);
    // useSubgrids(interpolated, index, fieldData);

    calcDifferencesOdd(indexU, fieldU, data, getSimData().deltax_R);

    // for j = [c111 c221 c212 c122]% Update components on even grids.
    //     W = -sumcell([dxU(Ix{j}),dyU(Iy{j}),dzU(Iz{j})],...
    //         [par.Mx(j,Ix{j}),par.My(j,Iy{j}),par.Mz(j,Iz{j})]);
    //     for k = 1:2                         % Perform two half-steps.
    //         if j==1        % Zeroth moment decays by absorption only.
    //             U{j} = U{j}+dt/2*(W+Q{j}-sA{1,1,1}.*U{j}).*EA;
    //         else                  % All other moments decay normally.
    //             U{j} = U{j}+dt/2*(W+Q{j}-...
    //                 sT{gtx(j),gty(j),gtz(j),par.mom_order(j)}.*U{j}).*...
    //                 ET{gtx(j),gty(j),gtz(j),par.mom_order(j)};
    //         end
    //     end
    // end
    for (auto j : data.c111) {
      real_t sumW = calcSumW(j, data);
      if (j == 0) {
        fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + interpolated[data_q][grid111] - interpolated[data_sa][grid111] * fieldU.getCell(indexU, j)) * interpolated[data_ea][grid111];
        fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + interpolated[data_q][grid111] - interpolated[data_sa][grid111] * fieldU.getCell(indexU, j)) * interpolated[data_ea][grid111];
      } else {
        real_t q = 0.0;
        fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid111] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid111];
        fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid111] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid111];
      }
    }
    for (auto j : data.c221) {
      real_t sumW = calcSumW(j, data);
      real_t q    = 0.0;
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid221] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid221];
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid221] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid221];
    }
    for (auto j : data.c212) {
      real_t sumW = calcSumW(j, data);
      real_t q    = 0.0;
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid212] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid212];
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid212] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid212];
    }
    for (auto j : data.c122) {
      real_t sumW = calcSumW(j, data);
      real_t q    = 0.0;
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid122] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid122];
      fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - interpolated[data_st][grid122] * fieldU.getCell(indexU, j)) * interpolated[data_et][grid122];
    }

    __asm volatile("# LLVM-MCA-END starmap3d_2");
  }
}

// inline void function(real_t& cell, unsigned int j, DataStaRMAP& data, unsigned int grid) {
//   real_t sumW = calcSumW(j, data);
//   real_t q    = 0.0;
//   cell += 0.5 * getSimData().deltat * (sumW + q - data.interpolated[data_st][grid] * cell) * data.interpolated[data_et][grid];
//   cell += 0.5 * getSimData().deltat * (sumW + q - data.interpolated[data_st][grid] * cell) * data.interpolated[data_et][grid];
// }

void SweepStaRMAP3D2::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace starmap
