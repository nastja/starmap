/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#include "sweep_starmap3dv9.h"
#include "helper/starmapmath.h"
#include "fmt/core.h"
#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/simd/simd.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <numeric>
#include <set>
#include <vector>

namespace starmap {

using namespace math;
using namespace nastja;
using namespace nastja::simd;
using namespace nastja::stencil;

void SweepStaRMAP3Dv9::init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) {
  Sweep::init(block, fieldpool);
}

void SweepStaRMAP3Dv9::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }

  auto& data = getSimData().initData<DataStaRMAP>();
  data.configure(getSimData().getConfig());
}

void SweepStaRMAP3Dv9::executeBlock(block::Block* block) {
  auto& fieldU    = block->getFieldByName("u")->get<real_t, true>();
  auto& fieldData = block->getFieldByName("data")->get<real_t, true>();
  auto& data      = getSimData().getData<DataStaRMAP>();

  const float64x4_t deltax_R = set_pd(getSimData().deltax_R);
  const float64x4_t deltath  = set_pd(0.5 * getSimData().deltat);

  auto* dU           = data.dUavx;
  auto* interpolated = data.interpolated;

  auto view = fieldU.createView<4>();
  for (const auto& cell : view) {
    __asm volatile("# LLVM-MCA-BEGIN starmap3d_1");

    auto index = view.getIndex(cell) / data.n_sys * 5;
    getInterpolated4(interpolated, index, fieldData, 3);
    // useMaingrid(interpolated, index, fieldData, 3);
    // useSubgrids(interpolated, index, fieldData, 3);
    __asm volatile("# LLVM-MCA-END starmap3d_1");
    __asm volatile("# LLVM-MCA-BEGIN starmap3d_2");

    for (int j = data.latticeEnd[0]; j < data.latticeEnd[1]; j++) {
      auto dxU = (view.load_unaligned(cell, Direction::R, j) - view.load(cell, j)) * deltax_R;
      auto dyU = (view.load(cell, Direction::U, j) - view.load(cell, j)) * deltax_R;
      auto dzU = (view.load(cell, Direction::F, j) - view.load(cell, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }

    for (int j = data.latticeEnd[1]; j < data.latticeEnd[2]; j++) {
      auto dxU = (view.load(cell, j) - view.load_unaligned(cell, Direction::L, j)) * deltax_R;
      auto dyU = (view.load(cell, j) - view.load(cell, Direction::D, j)) * deltax_R;
      auto dzU = (view.load(cell, Direction::F, j) - view.load(cell, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }

    for (int j = data.latticeEnd[2]; j < data.latticeEnd[3]; j++) {
      auto dxU = (view.load(cell, j) - view.load_unaligned(cell, Direction::L, j)) * deltax_R;
      auto dyU = (view.load(cell, Direction::U, j) - view.load(cell, j)) * deltax_R;
      auto dzU = (view.load(cell, j) - view.load(cell, Direction::B, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }

    for (int j = data.latticeEnd[3]; j < data.latticeEnd[4]; j++) {
      auto dxU = (view.load_unaligned(cell, Direction::R, j) - view.load(cell, j)) * deltax_R;
      auto dyU = (view.load(cell, j) - view.load(cell, Direction::D, j)) * deltax_R;
      auto dzU = (view.load(cell, j) - view.load(cell, Direction::B, j)) * deltax_R;

      store(dU + 4 * (3 * j + 0), dxU);
      store(dU + 4 * (3 * j + 1), dyU);
      store(dU + 4 * (3 * j + 2), dzU);
    }
    __asm volatile("# LLVM-MCA-END starmap3d_2");
    __asm volatile("# LLVM-MCA-BEGIN starmap3d_3");
    auto sT = load(interpolated + 4 * (8 * data_st + grid211));
    auto eT = load(interpolated + 4 * (8 * data_et + grid211));
    // for (auto j : data.c211) {
    for (int j = data.latticeEnd[4]; j < data.latticeEnd[5]; j++) {
      auto sumW = calcSumW_avx(j, data);
      auto Uj   = view.load(cell, j);
      Uj += deltath * (sumW - sT * Uj) * eT;
      view.store(cell, Uj, j);
    }

    sT = load(interpolated + 4 * (8 * data_st + grid121));
    eT = load(interpolated + 4 * (8 * data_et + grid121));
    // for (auto j : data.c121) {
    for (int j = data.latticeEnd[5]; j < data.latticeEnd[6]; j++) {
      auto sumW = calcSumW_avx(j, data);
      auto Uj   = view.load(cell, j);
      Uj += deltath * (sumW - sT * Uj) * eT;
      view.store(cell, Uj, j);
    }

    sT = load(interpolated + 4 * (8 * data_st + grid112));
    eT = load(interpolated + 4 * (8 * data_et + grid112));
    // for (auto j : data.c112) {
    for (int j = data.latticeEnd[6]; j < data.latticeEnd[7]; j++) {
      auto sumW = calcSumW_avx(j, data);
      auto Uj   = view.load(cell, j);
      Uj += deltath * (sumW - sT * Uj) * eT;
      view.store(cell, Uj, j);
    }

    sT = load(interpolated + 4 * (8 * data_st + grid222));
    eT = load(interpolated + 4 * (8 * data_et + grid222));
    // for (auto j : data.c222) {
    for (int j = data.latticeEnd[7]; j < data.latticeEnd[8]; j++) {
      auto sumW = calcSumW_avx(j, data);
      auto Uj   = view.load(cell, j);
      Uj += deltath * (sumW - sT * Uj) * eT;
      view.store(cell, Uj, j);
    }

    __asm volatile("# LLVM-MCA-END starmap3d_3");
  }
}

void SweepStaRMAP3Dv9::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

void SweepStaRMAP3Dv9::setup(const block::BlockRegister& /*blockRegister*/) {
  if (isSetuped) return;
  isSetuped  = true;
  auto& data = getSimData().getData<DataStaRMAP>();
  setupClosure(data);
  makegrid(data);
  reorderIndizes4(data);

  data.dUavx        = field::allocate<real_t>(4 * 3 * data.n_sys);
  data.interpolated = field::allocate<real_t>(4 * 8 * 5);
}

}  // namespace starmap
