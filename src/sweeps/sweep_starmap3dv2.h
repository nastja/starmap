/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/datatypes.h"
#include <array>
#include <vector>

namespace starmap {

/**
 * Class for sweep StaRMAP.
 *
 * @ingroup sweeps
 */
class SweepStaRMAP3Dv2 : public nastja::Sweep {
public:
  explicit SweepStaRMAP3Dv2() {
    registerFieldname("u");
    registerFieldname("data");
  }

  ~SweepStaRMAP3Dv2() override                  = default;
  SweepStaRMAP3Dv2(const SweepStaRMAP3Dv2&)     = delete;
  SweepStaRMAP3Dv2(SweepStaRMAP3Dv2&&) noexcept = default;
  SweepStaRMAP3Dv2& operator=(const SweepStaRMAP3Dv2&) = delete;
  SweepStaRMAP3Dv2& operator=(SweepStaRMAP3Dv2&&) = delete;

  void init(nastja::block::Block* block, const nastja::Pool<nastja::field::FieldProperties>& fieldpool) override;
  void init(const nastja::block::BlockRegister& blockRegister, const nastja::Pool<nastja::field::FieldProperties>& fieldpool) override;
  void executeBlock(nastja::block::Block* block) override;
  void execute(const nastja::block::BlockRegister& blockRegister) override;
  void setup(const nastja::block::BlockRegister& blockRegister) override;

private:
  bool isSetuped{false};
};

}  // namespace starmap
