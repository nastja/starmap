/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_starmap_substep2.h"
#include "helper/starmapmath.h"
#include "fmt/core.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <set>
#include <vector>

namespace starmap {

using namespace nastja;
using namespace nastja::stencil;

int coord0(int s, int i, int j);

void SweepStaRMAP2::executeBlock(block::Block* block) {
  auto& fieldU    = block->getFieldByName("u")->get<real_t>();
  auto& fieldData = block->getFieldByName("data")->get<real_t>();
  auto& data      = getSimData().getData<DataStaRMAP>();

  const auto& boundarySize = fieldU.getBoundarySize();

  std::vector<real_t> dxU(data.n_sys);
  std::vector<real_t> dyU(data.n_sys);

  for (unsigned long z = boundarySize[2]; z < fieldU.getSize(2) - boundarySize[2]; z++) {
    for (unsigned long y = boundarySize[1]; y < fieldU.getSize(1) - boundarySize[1]; y++) {
      for (unsigned long x = boundarySize[0]; x < fieldU.getSize(0) - boundarySize[0]; x++) {
        auto indexU = fieldU.getIndex(x, y, z);
        auto index  = fieldData.getIndex(x, y, z);

        // case 2
        // for j = c21       % Compute update at (1,1) and (2,2) from (2,1).
        //     dxU{j} = diff(U{j}(extendx{2},:),[],1)/h(1);
        //     dyU{j} = diff(U{j}(:,extendy{1}),[],2)/h(2);
        // end
        for (auto j : data.c21) {
          dxU[j] = (fieldU.getCell(indexU, j) - fieldU.getCell(indexU, Direction::L, j)) * getSimData().deltax_R;
          dyU[j] = (fieldU.getCell(indexU, Direction::U, j) - fieldU.getCell(indexU, j)) * getSimData().deltax_R;
        }
        // for j = c12       % Compute update at (2,2) and (1,1) from (2,1).
        //     dxU{j} = diff(U{j}(extendx{1},:),[],1)/h(1);
        //     dyU{j} = diff(U{j}(:,extendy{2}),[],2)/h(2);
        // end
        for (auto j : data.c12) {
          dxU[j] = (fieldU.getCell(indexU, Direction::R, j) - fieldU.getCell(indexU, j)) * getSimData().deltax_R;
          dyU[j] = (fieldU.getCell(indexU, j) - fieldU.getCell(indexU, Direction::D, j)) * getSimData().deltax_R;
        }
        // for j = [c11 c22]   % Update components on (1,1) and (2,2) grids.
        //   W = -sumcell([dxU(Ix{j}),dyU(Iy{j})],[par.Mx(j,Ix{j}),par.My(j,Iy{j})]);
        //   if j==1        % Zeroth moment decays by absorption only.
        //     U{j} = U{j}+dt/2*(W+Q{j}-sA1.*U{j}).*EA;
        //   else                  % All other moments decay normally.
        //     U{j} = U{j}+dt/2*(W+Q{j}-sT1{gtx(j),gty(j),par.mom_order(j)}.*U{j}).*ET{gtx(j),gty(j),par.mom_order(j)};
        //   end
        // end
        for (auto j : data.c11) {
          real_t sumW = 0.0;
          for (unsigned int i = 0; i < MatrixWidth; i++) {
            sumW -= dxU[data.Ix[MatrixWidth * j + i]] * data.Mx[MatrixWidth * j + i];
            sumW -= dyU[data.Iy[MatrixWidth * j + i]] * data.My[MatrixWidth * j + i];
          }
          if (j == 0) {
            fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + fieldData.getCell(index, 0) - fieldData.getCell(index, 1) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 3);
            fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + fieldData.getCell(index, 0) - fieldData.getCell(index, 1) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 3);
          } else {
            real_t q = 0.0;
            fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - fieldData.getCell(index, 2) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 4);
            fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - fieldData.getCell(index, 2) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 4);
          }
        }
        for (auto j : data.c22) {
          real_t sumW = 0.0;
          for (unsigned int i = 0; i < MatrixWidth; i++) {
            sumW -= dxU[data.Ix[MatrixWidth * j + i]] * data.Mx[MatrixWidth * j + i];
            sumW -= dyU[data.Iy[MatrixWidth * j + i]] * data.My[MatrixWidth * j + i];
          }
          real_t q = 0.0;
          if (j == 0) q = fieldData.getCell(index, 0);
          fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - fieldData.getCell(index, 2) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 4);
          fieldU.getCell(indexU, j) += 0.5 * getSimData().deltat * (sumW + q - fieldData.getCell(index, 2) * fieldU.getCell(indexU, j)) * fieldData.getCell(index, 4);
        }
      }
    }
  }
}

void SweepStaRMAP2::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace starmap
