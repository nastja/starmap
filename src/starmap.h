/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"

namespace starmap {

using namespace nastja::config::literals;

class AppStaRMAP : public nastja::Application {
public:
  explicit AppStaRMAP(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {
    is2D = simdata.getConfig().getValue<int>("StaRMAP.2D"_jptr, false);
    /// @key{Settings.loopvariant, int, 0}
    /// Chooses the loop variant.
    variant = simdata.getConfig().getValue<int>("Settings.loopvariant"_jptr, 0);
  }

  ~AppStaRMAP() override            = default;
  AppStaRMAP(const AppStaRMAP&)     = delete;
  AppStaRMAP(AppStaRMAP&&) noexcept = default;
  AppStaRMAP& operator=(const AppStaRMAP&) = delete;
  AppStaRMAP& operator=(AppStaRMAP&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;

  int variant;
  bool is2D;
};

}  // namespace starmap
