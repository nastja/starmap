/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "starmapprecalc.h"
#include "lib/field/field.h"
#include <cmath>

namespace starmap {

using namespace nastja;

inline real_t expm1div(real_t x) {
  if (x == 0) return 1.0;
  return std::expm1(x) / x;
}

template <unsigned int TSplitX>
void calculate(field::Field<real_t, TSplitX>& field, SimData& simData) {
  auto deltat = simData.deltat;

  auto view = field.template createView<1>();
  for (auto cell = view.begin(); cell != view.end(); ++cell) {
    auto v = view.getVec(*cell);
    // remapping and precalculation: q, sigma_a, sigma_s -> q, sigma_a, ea, sigma_t, et
    v[3] = v[2] + v[1];
    v[2] = expm1div(-v[1] * 0.5 * deltat);
    v[4] = expm1div(-v[3] * 0.5 * deltat);
  }
}

void StaRMAPprecalc::executeBlock(block::Block* block) {
  if (auto* field = block->getFieldByName("data")->getPtr<real_t>()) {
    calculate(*field, getSimData());
  } else if (auto* field = block->getFieldByName("data")->getPtr<real_t, true>()) {
    calculate(*field, getSimData());
  }
}

void StaRMAPprecalc::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}
}  // namespace starmap
