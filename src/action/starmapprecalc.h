/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/datatypes.h"
#include "lib/logger.h"

namespace starmap {

class StaRMAPprecalc : public nastja::Action {
public:
  explicit StaRMAPprecalc(const std::string& fieldname)
      : Action{} {
    registerFieldname(fieldname);
  }

  ~StaRMAPprecalc() override                = default;
  StaRMAPprecalc(const StaRMAPprecalc&)     = delete;
  StaRMAPprecalc(StaRMAPprecalc&&) noexcept = default;
  StaRMAPprecalc& operator=(const StaRMAPprecalc&) = delete;
  StaRMAPprecalc& operator=(StaRMAPprecalc&&) = delete;

  using Action::init;
  void init(const nastja::block::BlockRegister& /*blockRegister*/, const nastja::Pool<nastja::field::FieldProperties>& /*fieldpool*/) override{};
  void executeBlock(nastja::block::Block* block) override;
  void execute(const nastja::block::BlockRegister& blockRegister) override;
  void setup(const nastja::block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }
};

}  // namespace starmap
