# Single Application targets using the library.
#
set(STARMAP_SOURCES
  starmap.cpp
  action/starmapprecalc.cpp
  helper/starmapmath.cpp
  sweeps/sweep_starmap.cpp
  sweeps/sweep_starmap3d.cpp
  sweeps/sweep_starmap3dv2.cpp
  sweeps/sweep_starmap3dv3.cpp
  sweeps/sweep_starmap3dv4.cpp
  sweeps/sweep_starmap3dv5.cpp
  sweeps/sweep_starmap3dv6.cpp
  sweeps/sweep_starmap3dv7.cpp
  sweeps/sweep_starmap3dv8.cpp
  sweeps/sweep_starmap3dv9.cpp
  sweeps/sweep_starmap3d_substep2.cpp
  sweeps/sweep_starmap3dv2_substep2.cpp
  sweeps/sweep_starmap3dv3_substep2.cpp
  sweeps/sweep_starmap3dv4_substep2.cpp
  sweeps/sweep_starmap3dv5_substep2.cpp
  sweeps/sweep_starmap3dv6_substep2.cpp
  sweeps/sweep_starmap3dv7_substep2.cpp
  sweeps/sweep_starmap3dv8_substep2.cpp
  sweeps/sweep_starmap3dv9_substep2.cpp
  sweeps/sweep_starmap_substep2.cpp
)

# NAStJA StaRMAP target using the library.
#
add_executable(nastja_starmap ${STARMAP_SOURCES})
target_link_libraries(nastja_starmap PRIVATE nastjalibrary)
target_include_directories(nastja_starmap PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
set_target_properties(nastja_starmap PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}")

if(HAS_IPO_SUPPORT)
  set_target_properties(nastja_starmap PROPERTIES INTERPROCEDURAL_OPTIMIZATION TRUE)
endif()
