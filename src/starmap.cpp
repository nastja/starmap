/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "starmap.h"
#include "action/starmapprecalc.h"
#include "sweeps/sweep_starmap.h"
#include "sweeps/sweep_starmap3d.h"
#include "sweeps/sweep_starmap3d_substep2.h"
#include "sweeps/sweep_starmap3dv2.h"
#include "sweeps/sweep_starmap3dv2_substep2.h"
#include "sweeps/sweep_starmap3dv3.h"
#include "sweeps/sweep_starmap3dv3_substep2.h"
#include "sweeps/sweep_starmap3dv4.h"
#include "sweeps/sweep_starmap3dv4_substep2.h"
#include "sweeps/sweep_starmap3dv5.h"
#include "sweeps/sweep_starmap3dv5_substep2.h"
#include "sweeps/sweep_starmap3dv6.h"
#include "sweeps/sweep_starmap3dv6_substep2.h"
#include "sweeps/sweep_starmap3dv7.h"
#include "sweeps/sweep_starmap3dv7_substep2.h"
#include "sweeps/sweep_starmap3dv8.h"
#include "sweeps/sweep_starmap3dv8_substep2.h"
#include "sweeps/sweep_starmap3dv9.h"
#include "sweeps/sweep_starmap3dv9_substep2.h"
#include "sweeps/sweep_starmap_substep2.h"
#include "lib/action/actionregister.h"
#include "lib/nastja.h"
#include "lib/stencil/D3C7.h"
#include <memory>

NASTJA_SINGLE_APP(starmap::AppStaRMAP)

namespace starmap {

using namespace nastja;

void AppStaRMAP::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  auto n_mom      = simdata.getConfig().getValue<unsigned int>("StaRMAP.n_mom"_jptr);
  n_mom += 1;
  n_mom *= n_mom;
  unsigned int ft = field::FieldType::real;
  if (variant == 8) {
    ft |= field::FieldType::splitX;
  }
  fieldpool.registerEmplace("u", stencil::D3C7{}, ft, n_mom, 1);
  fieldpool.registerEmplace("data", stencil::D3C7{}, ft, 5, 1);
}

void AppStaRMAP::registerActions() {
  auto& actionpool = blockmanager.getActionPool();

  if (is2D) {
    actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP>());
    actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP2>());
  } else {
    switch (variant) {
      case 1:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv2>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v2>());
        break;
      case 2:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv3>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v3>());
        break;
      case 3:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv4>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v4>());
        break;
      case 4:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv5>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v5>());
        break;
      case 5:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv6>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v6>());
        break;
      case 6:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv7>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v7>());
        break;
      case 7:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv8>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v8>());
        break;
      case 8:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3Dv9>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2v9>());
        break;
      default:
        actionpool.registerElement("Sweep:StaRMAPstep1", std::make_unique<SweepStaRMAP3D>());
        actionpool.registerElement("Sweep:StaRMAPstep2", std::make_unique<SweepStaRMAP3D2>());
    }
    actionpool.registerElement("Filling:post", std::make_unique<StaRMAPprecalc>("data"));
  }

  registerWriterElements();
}

void AppStaRMAP::registerActionList() {
  blockmanager.registerAction("Sweep:StaRMAPstep1");
  blockmanager.registerAction("BC:u");
  blockmanager.registerAction("Sweep:StaRMAPstep2");
  blockmanager.registerAction("BC:u");
  blockmanager.registerAction("Sweep:StaRMAPstep1");
  blockmanager.registerAction("BC:u");
  blockmanager.registerActionWriters();
}

bool AppStaRMAP::init() {
  if (Application::init()) return true;

  blockmanager.setupActions();
  blockmanager.executeActionWriterOnBlocks();

  return false;
}

}  // namespace starmap
