/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "starmapmath.h"
#include "helper/datastarmap.h"
#include "lib/datatypes.h"
#include "lib/stencil/direction.h"
#include <vector>

namespace starmap {
namespace math {

using namespace nastja;
using namespace nastja::stencil;

void addIndices(std::set<int>& c, const std::vector<int>& I, const std::set<int>& idx) {
  for (const auto& j : idx) {
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      int index = I[MatrixWidth * j + i];
      if (index != -1) c.insert(index);
    }
  }
}

/**
 * Gets the interpolated.
 *
 * xyz
 * 111 0
 * 112 1
 * 121 2
 * 122 3
 * 211 4
 * 212 5
 * 221 6
 * 222 7
 *
 * @param res    The resource.
 * @param index  The index.
 * @param field  The field.
 */
void getInterpolated(std::array<std::array<real_t, 8>, 5>& res, const unsigned long index, const field::Field<real_t, 1>& field, int start /*= 0*/) {
  for (int i = start; i < 5; i++) {
    res[i][grid111] = field.getCell(index, i);
    res[i][grid112] = 0.5 * (field.getCell(index, i) + field.getCell(index, Direction::F, i));
    res[i][grid121] = 0.5 * (field.getCell(index, i) + field.getCell(index, Direction::U, i));
    res[i][grid211] = 0.5 * (field.getCell(index, i) + field.getCell(index, Direction::R, i));

    res[i][grid122] = 0.5 * (res[i][grid121] + 0.5 * (field.getCell(index, Direction::F, i) + field.getCell(index, Direction::FU, i)));
    res[i][grid212] = 0.5 * (res[i][grid112] + 0.5 * (field.getCell(index, Direction::R, i) + field.getCell(index, Direction::FR, i)));
    res[i][grid221] = 0.5 * (res[i][grid211] + 0.5 * (field.getCell(index, Direction::U, i) + field.getCell(index, Direction::UR, i)));

    res[i][grid222] = 0.5 * (res[i][grid122] + 0.5 * (0.5 * (field.getCell(index, Direction::R, i) + field.getCell(index, Direction::FR, i)) +
                                                      0.5 * (field.getCell(index, Direction::UR, i) + field.getCell(index, Direction::FUR, i))));
  }
}

void getInterpolated4(real_t* res, const unsigned long index, field::Field<real_t, 4>& field, int start /*= 0*/) {
  auto view    = field.createView<4>();
  real_t* cell = field.getCellBasePtr() + index;
  for (int i = start; i < 5; i++) {
    simd::store(res + 4 * (8 * i + grid111), view.load(cell, i));
    simd::store(res + 4 * (8 * i + grid112), 0.5 * (view.load(cell, i) + view.load(cell, Direction::F, i)));
    simd::store(res + 4 * (8 * i + grid121), 0.5 * (view.load(cell, i) + view.load(cell, Direction::U, i)));
    simd::store(res + 4 * (8 * i + grid211), 0.5 * (view.load(cell, i) + view.load_unaligned(cell, Direction::R, i)));

    simd::store(res + 4 * (8 * i + grid122), 0.5 * (simd::load(res + 4 * (8 * i + grid121)) + 0.5 * (view.load(cell, Direction::F, i) + view.load(cell, Direction::FU, i))));
    simd::store(res + 4 * (8 * i + grid212), 0.5 * (simd::load(res + 4 * (8 * i + grid112)) + 0.5 * (view.load_unaligned(cell, Direction::R, i) + view.load_unaligned(cell, Direction::FR, i))));
    simd::store(res + 4 * (8 * i + grid221), 0.5 * (simd::load(res + 4 * (8 * i + grid211)) + 0.5 * (view.load(cell, Direction::U, i) + view.load_unaligned(cell, Direction::UR, i))));

    simd::store(res + 4 * (8 * i + grid222), 0.5 * (simd::load(res + 4 * (8 * i + grid122)) + 0.5 * (0.5 * (view.load_unaligned(cell, Direction::R, i) + view.load_unaligned(cell, Direction::FR, i)) +
                                                                                                     0.5 * (view.load_unaligned(cell, Direction::UR, i) + view.load_unaligned(cell, Direction::FUR, i)))));
  }
}

void useMaingrid(std::array<std::array<real_t, 8>, 5>& res, const unsigned long index, const field::Field<real_t, 1>& field, int start /*= 0*/) {
  for (int i = start; i < 5; i++) {
    for (int j = 0; j < 8; j++) {
      res[i][j] = field.getCell(index, i);
    }
  }
}

void useSubgrids(std::array<std::array<real_t, 8>, 5>& res, const unsigned long index, const field::Field<real_t, 1>& field, int start /*= 0*/) {
  for (int i = start; i < 5; i++) {
    for (int j = 0; j < 8; j++) {
      res[i][j] = field.getCell(index, i * 8 + j);
    }
  }
}

void setupClosure(DataStaRMAP& data) {
  if (data.closure == Closure::P) {
    setupClosureP(data);
  } else {
    throw(std::invalid_argument("Unknown closure."));
  }
}

void setupClosureP(DataStaRMAP& data) {
  // n_sys = (n_mom+1)^2; % number of system components in 3D
  // Mx = sparse(zeros(n_sys)); My = Mx; Mz = Mx; s = size(Mx);
  data.n_sys = (data.n_mom + 1) * (data.n_mom + 1);  // number of system components

  data.fullMx.resize(data.n_sys * data.n_sys);
  data.fullMy.resize(data.n_sys * data.n_sys);
  data.fullMz.resize(data.n_sys * data.n_sys);

  // for m = 1:n_mom
  //     i = 1:2*m-1; p = (m-1)^2+i;
  //     v = F(m,m+1-ceil(i/2));
  //     Mx(sub2ind(s,p,p+2*m-1)) = v;
  //     My(sub2ind(s,p,p+2*m-1-(-1).^i)) = -(-1).^i.*v;
  //     v = B(m,m-ceil(i/2));
  //     Mz(sub2ind(s,p,p+2*m+1)) = v;
  //     i = 1:2*m-3; p = (m-1)^2+i;
  //     v = D(m,m-1-ceil(i/2));
  //     Mx(sub2ind(s,p,p+2*m+3)) = -v;
  //     if m>1, p(end) = p(end)+1; i(end) = i(end)+1; end
  //     My(sub2ind(s,p,p+2*m+3-(-1).^i)) = -(-1).^i.*v;
  // end
  for (unsigned int m = 1; m <= data.n_mom; m++) {
    for (unsigned int i = 1; i < 2 * m; i++) {
      int p    = (m - 1) * (m - 1) + i;
      real_t v = F(m, m + 1 - std::ceil(i / 2.0));

      data.fullMx[coord(data.n_sys, p, p + 2 * m - 1)]                         = v;
      data.fullMy[coord(data.n_sys, p, p + 2 * m - 1 - (i % 2 == 0 ? 1 : -1))] = -(i % 2 == 0 ? v : -v);
      v                                                                        = B(m, m - std::ceil(i / 2.0));
      data.fullMz[coord(data.n_sys, p, p + 2 * m + 1)]                         = v;
    }
    if (m == 1) continue;
    for (unsigned int i = 1; i <= 2 * m - 3; i++) {
      unsigned int p = (m - 1) * (m - 1) + i;
      real_t v       = D(m, m - 1 - std::ceil(i / 2.0));

      data.fullMx[coord(data.n_sys, p, p + 2 * m + 3)] = -v;
      if (m > 1 && i == 2 * m - 3) {
        p++;
        i++;
      }
      data.fullMy[coord(data.n_sys, p, p + 2 * m + 3 - (i % 2 == 0 ? 1 : -1))] = -(i % 2 == 0 ? v : -v);
    }
  }

  // m = 1:n_mom; i = m.^2;
  // Mx(i,:) = sqrt(2)*Mx(i,:); My(i,:) = sqrt(2)*My(i,:);
  for (unsigned int m = 1; m <= data.n_mom; m++) {
    unsigned int i = m * m;
    for (unsigned int j = 0; j < data.n_sys; j++) {
      data.fullMx[coord(data.n_sys, i, 1) + j] *= M_SQRT2;
      data.fullMy[coord(data.n_sys, i, 1) + j] *= M_SQRT2;
    }
  }

  //
  // m = 2:n_mom; i = (m+1).^2;
  // Mx(:,i) = sqrt(2)*Mx(:,i); My(:,i) = sqrt(2)*My(:,i);
  for (unsigned int m = 2; m <= data.n_mom; m++) {
    unsigned int i = (m + 1) * (m + 1);
    for (unsigned int j = 0; j < data.n_sys; j++) {
      data.fullMx[coord(data.n_sys, 1 + j, i)] *= M_SQRT2;
      data.fullMy[coord(data.n_sys, 1 + j, i)] *= M_SQRT2;
    }
  }

  // Mx = full((Mx+Mx')/2); My = full((My+My')/2); % symmetry and factor 1/2
  // Mz = full(Mz+Mz');
  for (unsigned int i = 1; i <= data.n_sys; i++) {
    for (unsigned int j = i; j <= data.n_sys; j++) {
      real_t val = 0.5 * (data.fullMx[coord(data.n_sys, i, j)] + data.fullMx[coord(data.n_sys, j, i)]);

      data.fullMx[coord(data.n_sys, i, j)] = val;
      data.fullMx[coord(data.n_sys, j, i)] = val;

      val = 0.5 * (data.fullMy[coord(data.n_sys, i, j)] + data.fullMy[coord(data.n_sys, j, i)]);

      data.fullMy[coord(data.n_sys, i, j)] = val;
      data.fullMy[coord(data.n_sys, j, i)] = val;

      val = (data.fullMz[coord(data.n_sys, i, j)] + data.fullMz[coord(data.n_sys, j, i)]);

      data.fullMz[coord(data.n_sys, i, j)] = val;
      data.fullMz[coord(data.n_sys, j, i)] = val;
    }
  }

  // for (int i = 0; i < data.n_sys; i++) {
  //   for (int j = 0; j < data.n_sys; j++) {
  //     std::cout << fmt::format("{:.4f} ", Mx[coord0(data.n_sys, i, j)]);
  //   }
  //   std::cout << "\n";
  // }
}

void makegrid(DataStaRMAP& data) {
  data.Ix.resize(MatrixWidth * data.n_sys);
  data.Iy.resize(MatrixWidth * data.n_sys);
  data.Iz.resize(MatrixWidth * data.n_sys);
  std::fill(data.Ix.begin(), data.Ix.end(), -1);
  std::fill(data.Iy.begin(), data.Iy.end(), -1);
  std::fill(data.Iz.begin(), data.Iz.end(), -1);

  // Ix = cellfun(@find,num2cell(par.Mx',1),'Un',0);         % Grid dependency
  // Iy = cellfun(@find,num2cell(par.My',1),'Un',0);                % indices.
  // Iz = cellfun(@find,num2cell(par.Mz',1),'Un',0);
  for (unsigned int j = 0; j < data.n_sys; j++) {
    int cntx = 0;
    int cnty = 0;
    int cntz = 0;
    for (unsigned int i = 0; i < data.n_sys; i++) {
      if (data.fullMx[coord0(data.n_sys, j, i)] != 0.0) {
        data.Ix[MatrixWidth * j + cntx] = i;
        cntx++;
      }
      if (data.fullMy[coord0(data.n_sys, j, i)] != 0.0) {
        data.Iy[MatrixWidth * j + cnty] = i;
        cnty++;
      }
      if (data.fullMz[coord0(data.n_sys, j, i)] != 0.0) {
        data.Iz[MatrixWidth * j + cntz] = i;
        cntz++;
      }
    }
  }

  printIndex(data);

  // c111 = 1; c122 = []; c121 = []; c112 = [];        % First component is on
  // c211 = []; c222 = []; c221 = []; c212 = [];                  %(1,1)-grid.
  // while length([c111,c122,c121,c112,c211,c222,c221,c212])<n_sys % Determine
  // c11, c22, c21, c12
  std::set<int> cs111;
  std::set<int> cs121;
  std::set<int> cs211;
  std::set<int> cs221;
  std::set<int> cs112;
  std::set<int> cs122;
  std::set<int> cs212;
  std::set<int> cs222;
  cs111.insert(0);
  while (cs111.size() + cs121.size() + cs211.size() + cs221.size() + cs112.size() + cs122.size() + cs212.size() + cs222.size() < data.n_sys) {
    //     c211 = unique(vertcat(c211',Ix{c111},Iy{c221},Iz{c212})');  % indices
    addIndices(cs211, data.Ix, cs111);
    addIndices(cs211, data.Iy, cs221);
    addIndices(cs211, data.Iz, cs212);

    //     c121 = unique(vertcat(c121',Ix{c221},Iy{c111},Iz{c122})');  % of grid
    addIndices(cs121, data.Ix, cs221);
    addIndices(cs121, data.Iy, cs111);
    addIndices(cs121, data.Iz, cs122);

    //     c112 = unique(vertcat(c112',Ix{c212},Iy{c122},Iz{c111})');    % types
    addIndices(cs112, data.Ix, cs212);
    addIndices(cs112, data.Iy, cs122);
    addIndices(cs112, data.Iz, cs111);

    //     c221 = unique(vertcat(c221',Ix{c121},Iy{c211},Iz{c222})');     % from
    addIndices(cs221, data.Ix, cs121);
    addIndices(cs221, data.Iy, cs211);
    addIndices(cs221, data.Iz, cs222);

    //     c212 = unique(vertcat(c212',Ix{c112},Iy{c222},Iz{c211})');   % system
    addIndices(cs212, data.Ix, cs112);
    addIndices(cs212, data.Iy, cs222);
    addIndices(cs212, data.Iz, cs211);

    //     c122 = unique(vertcat(c122',Ix{c222},Iy{c112},Iz{c121})');% matrices.
    addIndices(cs122, data.Ix, cs222);
    addIndices(cs122, data.Iy, cs112);
    addIndices(cs122, data.Iz, cs121);

    //     c111 = unique(vertcat(c111',Ix{c211},Iy{c121},Iz{c112})');
    addIndices(cs111, data.Ix, cs211);
    addIndices(cs111, data.Iy, cs121);
    addIndices(cs111, data.Iz, cs112);

    //     c222 = unique(vertcat(c222',Ix{c122},Iy{c212},Iz{c221})');
    addIndices(cs222, data.Ix, cs122);
    addIndices(cs222, data.Iy, cs212);
    addIndices(cs222, data.Iz, cs221);
  }

  data.gtx.resize(data.n_sys);
  data.gty.resize(data.n_sys);
  data.gtz.resize(data.n_sys);
  data.mom_order.resize(data.n_sys);

  std::fill_n(data.gtx.begin(), data.n_sys, 1);
  std::fill_n(data.gty.begin(), data.n_sys, 1);
  std::fill_n(data.gtz.begin(), data.n_sys, 1);

  std::fill_n(data.mom_order.begin(), data.n_sys, 1);
  data.mom_order[0] = 0;

  // gtx([c211,c212,c221,c222]) = 2;                              % in x,y,z.
  // gty([c121,c122,c221,c222]) = 2;
  // gtz([c112,c122,c212,c222]) = 2;
  for (const auto& j : cs111) {
    data.c111.push_back(j);
  }

  for (const auto& j : cs211) {
    data.c211.push_back(j);
    data.gtx[j] = 2;
  }

  for (const auto& j : cs121) {
    data.gty[j] = 2;
    data.c121.push_back(j);
  }

  for (const auto& j : cs112) {
    data.gtz[j] = 2;
    data.c112.push_back(j);
  }

  for (const auto& j : cs221) {
    data.c221.push_back(j);
    data.gtx[j] = 2;
    data.gty[j] = 2;
  }

  for (const auto& j : cs212) {
    data.c212.push_back(j);
    data.gtx[j] = 2;
    data.gtz[j] = 2;
  }

  for (const auto& j : cs122) {
    data.gty[j] = 2;
    data.gtz[j] = 2;
    data.c122.push_back(j);
  }

  for (const auto& j : cs222) {
    data.gtx[j] = 2;
    data.gty[j] = 2;
    data.gtz[j] = 2;
    data.c222.push_back(j);
  }
  printSubgrids(data);
}

// used by v1
void compressMatrix(DataStaRMAP& data) {
  data.dxU.resize(data.n_sys);
  data.dyU.resize(data.n_sys);
  data.dzU.resize(data.n_sys);

  data.Mx.resize(MatrixWidth * data.n_sys);
  data.My.resize(MatrixWidth * data.n_sys);
  data.Mz.resize(MatrixWidth * data.n_sys);

  for (unsigned int j = 0; j < data.n_sys; j++) {
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (data.Ix[MatrixWidth * j + i] != -1) {
        data.Mx[MatrixWidth * j + i] = data.fullMx[coord0(data.n_sys, j, data.Ix[MatrixWidth * j + i])];
      } else {
        data.Ix[MatrixWidth * j + i] = i == 0 ? 0 : data.Ix[MatrixWidth * j + i - 1];
      }
      if (data.Iy[MatrixWidth * j + i] != -1) {
        data.My[MatrixWidth * j + i] = data.fullMy[coord0(data.n_sys, j, data.Iy[MatrixWidth * j + i])];
      } else {
        data.Iy[MatrixWidth * j + i] = i == 0 ? 0 : data.Iy[MatrixWidth * j + i - 1];
      }
      if (data.Iz[MatrixWidth * j + i] != -1) {
        data.Mz[MatrixWidth * j + i] = data.fullMz[coord0(data.n_sys, j, data.Iz[MatrixWidth * j + i])];
      } else {
        data.Iz[MatrixWidth * j + i] = i == 0 ? 0 : data.Iz[MatrixWidth * j + i - 1];
      }
    }
  }
}

// used by v3
void compressMatrix2(DataStaRMAP& data) {
  data.dxU.resize(data.n_sys);
  data.dyU.resize(data.n_sys);
  data.dzU.resize(data.n_sys);

  data.Mx.resize(MatrixWidth * data.n_sys);
  data.My.resize(MatrixWidth * data.n_sys);
  data.Mz.resize(MatrixWidth * data.n_sys);

  for (unsigned int j = 0; j < data.n_sys; j++) {
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (data.Ix[MatrixWidth * j + i] != -1) {
        data.Mx[MatrixWidth * j + i] = data.fullMx[coord0(data.n_sys, j, data.Ix[MatrixWidth * j + i])];
      }
      if (data.Iy[MatrixWidth * j + i] != -1) {
        data.My[MatrixWidth * j + i] = data.fullMy[coord0(data.n_sys, j, data.Iy[MatrixWidth * j + i])];
      }
      if (data.Iz[MatrixWidth * j + i] != -1) {
        data.Mz[MatrixWidth * j + i] = data.fullMz[coord0(data.n_sys, j, data.Iz[MatrixWidth * j + i])];
      }
    }
  }
}

std::vector<int> getIndizes(DataStaRMAP& data) {
  std::vector<int> indices;

  indices.insert(std::end(indices), std::begin(data.c111), std::end(data.c111));
  indices.insert(std::end(indices), std::begin(data.c221), std::end(data.c221));
  indices.insert(std::end(indices), std::begin(data.c212), std::end(data.c212));
  indices.insert(std::end(indices), std::begin(data.c122), std::end(data.c122));
  indices.insert(std::end(indices), std::begin(data.c211), std::end(data.c211));
  indices.insert(std::end(indices), std::begin(data.c121), std::end(data.c121));
  indices.insert(std::end(indices), std::begin(data.c112), std::end(data.c112));
  indices.insert(std::end(indices), std::begin(data.c222), std::end(data.c222));

  return indices;
}

std::vector<int> getReverseIndizes(std::vector<int> indices) {
  std::vector<int> rindices;
  rindices.resize(indices.size());
  for (unsigned int i = 0; i < indices.size(); i++) {
    rindices[indices[i]] = i;
  }

  return rindices;
}

void reorderSubgrids(DataStaRMAP& data) {
  data.latticeEnd[0] = 0;
  data.latticeEnd[1] = data.latticeEnd[0] + data.c111.size();
  data.latticeEnd[2] = data.latticeEnd[1] + data.c221.size();
  data.latticeEnd[3] = data.latticeEnd[2] + data.c212.size();
  data.latticeEnd[4] = data.latticeEnd[3] + data.c122.size();
  data.latticeEnd[5] = data.latticeEnd[4] + data.c211.size();
  data.latticeEnd[6] = data.latticeEnd[5] + data.c121.size();
  data.latticeEnd[7] = data.latticeEnd[6] + data.c112.size();
  data.latticeEnd[8] = data.latticeEnd[7] + data.c222.size();

  std::iota(data.c111.begin(), data.c111.end(), data.latticeEnd[0]);
  std::iota(data.c221.begin(), data.c221.end(), data.latticeEnd[1]);
  std::iota(data.c212.begin(), data.c212.end(), data.latticeEnd[2]);
  std::iota(data.c122.begin(), data.c122.end(), data.latticeEnd[3]);
  std::iota(data.c211.begin(), data.c211.end(), data.latticeEnd[4]);
  std::iota(data.c121.begin(), data.c121.end(), data.latticeEnd[5]);
  std::iota(data.c112.begin(), data.c112.end(), data.latticeEnd[6]);
  std::iota(data.c222.begin(), data.c222.end(), data.latticeEnd[7]);
}

// used by v2
// c111, c221, c212, c122, c211, c121, c112, c222
void reorderIndizes(DataStaRMAP& data) {
  auto indices  = getIndizes(data);
  auto rindices = getReverseIndizes(indices);

  data.dxU.resize(data.n_sys);
  data.dyU.resize(data.n_sys);
  data.dzU.resize(data.n_sys);

  data.Mx.resize(MatrixWidth * data.n_sys);
  data.My.resize(MatrixWidth * data.n_sys);
  data.Mz.resize(MatrixWidth * data.n_sys);

  auto Ix = data.Ix;
  auto Iy = data.Iy;
  auto Iz = data.Iz;

  for (unsigned int j = 0; j < data.n_sys; j++) {
    int orgj = indices[j];
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (Ix[MatrixWidth * orgj + i] != -1) {
        data.Ix[MatrixWidth * j + i] = rindices[Ix[MatrixWidth * orgj + i]];
        data.Mx[MatrixWidth * j + i] = data.fullMx[coord0(data.n_sys, orgj, Ix[MatrixWidth * orgj + i])];
      } else {
        data.Ix[MatrixWidth * j + i] = i == 0 ? 0 : data.Ix[MatrixWidth * j + i - 1];
      }
      if (Iy[MatrixWidth * orgj + i] != -1) {
        data.Iy[MatrixWidth * j + i] = rindices[Iy[MatrixWidth * orgj + i]];
        data.My[MatrixWidth * j + i] = data.fullMy[coord0(data.n_sys, orgj, Iy[MatrixWidth * orgj + i])];
      } else {
        data.Iy[MatrixWidth * j + i] = i == 0 ? 0 : data.Iy[MatrixWidth * j + i - 1];
      }
      if (Iz[MatrixWidth * orgj + i] != -1) {
        data.Iz[MatrixWidth * j + i] = rindices[Iz[MatrixWidth * orgj + i]];
        data.Mz[MatrixWidth * j + i] = data.fullMz[coord0(data.n_sys, orgj, Iz[MatrixWidth * orgj + i])];
      } else {
        data.Iz[MatrixWidth * j + i] = i == 0 ? 0 : data.Iz[MatrixWidth * j + i - 1];
      }
    }
  }

  reorderSubgrids(data);

  printMapping(indices, rindices);
  printIndex(data);
  printSubgrids(data);
}

// used by v4 v6
void reorderIndizes2(DataStaRMAP& data) {
  auto indices  = getIndizes(data);
  auto rindices = getReverseIndizes(indices);

  data.dxU.resize(data.n_sys);
  data.dyU.resize(data.n_sys);
  data.dzU.resize(data.n_sys);

  data.Mx.resize(MatrixWidth * data.n_sys);
  data.My.resize(MatrixWidth * data.n_sys);
  data.Mz.resize(MatrixWidth * data.n_sys);

  auto Ix = data.Ix;
  auto Iy = data.Iy;
  auto Iz = data.Iz;

  for (unsigned int j = 0; j < data.n_sys; j++) {
    int orgj = indices[j];
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (Ix[MatrixWidth * orgj + i] != -1) {
        data.Ix[MatrixWidth * j + i] = rindices[Ix[MatrixWidth * orgj + i]];
        data.Mx[MatrixWidth * j + i] = data.fullMx[coord0(data.n_sys, orgj, Ix[MatrixWidth * orgj + i])];
      } else {
        data.Ix[MatrixWidth * j + i] = -1;
      }
      if (Iy[MatrixWidth * orgj + i] != -1) {
        data.Iy[MatrixWidth * j + i] = rindices[Iy[MatrixWidth * orgj + i]];
        data.My[MatrixWidth * j + i] = data.fullMy[coord0(data.n_sys, orgj, Iy[MatrixWidth * orgj + i])];
      } else {
        data.Iy[MatrixWidth * j + i] = -1;
      }
      if (Iz[MatrixWidth * orgj + i] != -1) {
        data.Iz[MatrixWidth * j + i] = rindices[Iz[MatrixWidth * orgj + i]];
        data.Mz[MatrixWidth * j + i] = data.fullMz[coord0(data.n_sys, orgj, Iz[MatrixWidth * orgj + i])];
      } else {
        data.Iz[MatrixWidth * j + i] = -1;
      }
    }
  }

  reorderSubgrids(data);

  printMapping(indices, rindices);
  printIndex(data);
  printSubgrids(data);
}

// used by v5
void reorderIndizes3(DataStaRMAP& data) {
  auto indices  = getIndizes(data);
  auto rindices = getReverseIndizes(indices);

  data.dxU.resize(data.n_sys);
  data.dyU.resize(data.n_sys);
  data.dzU.resize(data.n_sys);

  data.Mx.resize(MatrixWidth * data.n_sys);
  data.My.resize(MatrixWidth * data.n_sys);
  data.Mz.resize(MatrixWidthZ * data.n_sys);

  auto Ix = data.Ix;
  auto Iy = data.Iy;
  auto Iz = data.Iz;

  data.Iz.resize(MatrixWidthZ * data.n_sys);

  for (unsigned int j = 0; j < data.n_sys; j++) {
    int orgj = indices[j];
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (Ix[MatrixWidth * orgj + i] != -1) {
        data.Ix[MatrixWidth * j + i] = rindices[Ix[MatrixWidth * orgj + i]];
        data.Mx[MatrixWidth * j + i] = data.fullMx[coord0(data.n_sys, orgj, Ix[MatrixWidth * orgj + i])];
      } else {
        data.Ix[MatrixWidth * j + i] = -1;
      }
      if (Iy[MatrixWidth * orgj + i] != -1) {
        data.Iy[MatrixWidth * j + i] = rindices[Iy[MatrixWidth * orgj + i]];
        data.My[MatrixWidth * j + i] = data.fullMy[coord0(data.n_sys, orgj, Iy[MatrixWidth * orgj + i])];
      } else {
        data.Iy[MatrixWidth * j + i] = -1;
      }
    }
    for (unsigned int i = 0; i < MatrixWidthZ; i++) {
      if (Iz[MatrixWidth * orgj + i] != -1) {
        data.Iz[MatrixWidthZ * j + i] = rindices[Iz[MatrixWidth * orgj + i]];
        data.Mz[MatrixWidthZ * j + i] = data.fullMz[coord0(data.n_sys, orgj, Iz[MatrixWidth * orgj + i])];
      } else {
        data.Iz[MatrixWidthZ * j + i] = -1;
      }
    }
  }
  reorderSubgrids(data);

  printMapping(indices, rindices);
  printIndex(data);
  printSubgrids(data);
}

//used by v7 v9
void reorderIndizes4(DataStaRMAP& data) {
  auto indices  = getIndizes(data);
  auto rindices = getReverseIndizes(indices);

  data.dU.resize(3 * data.n_sys);

  data.Mx.resize(MatrixWidth * data.n_sys);
  data.My.resize(MatrixWidth * data.n_sys);
  data.Mz.resize(MatrixWidth * data.n_sys);

  auto Ix = data.Ix;
  auto Iy = data.Iy;
  auto Iz = data.Iz;

  for (unsigned int j = 0; j < data.n_sys; j++) {
    int orgj = indices[j];
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (Ix[MatrixWidth * orgj + i] != -1) {
        data.Ix[MatrixWidth * j + i] = 3 * rindices[Ix[MatrixWidth * orgj + i]] + 0;
        data.Mx[MatrixWidth * j + i] = data.fullMx[coord0(data.n_sys, orgj, Ix[MatrixWidth * orgj + i])];
      } else {
        data.Ix[MatrixWidth * j + i] = -1;
      }
      if (Iy[MatrixWidth * orgj + i] != -1) {
        data.Iy[MatrixWidth * j + i] = 3 * rindices[Iy[MatrixWidth * orgj + i]] + 1;
        data.My[MatrixWidth * j + i] = data.fullMy[coord0(data.n_sys, orgj, Iy[MatrixWidth * orgj + i])];
      } else {
        data.Iy[MatrixWidth * j + i] = -1;
      }
      if (Iz[MatrixWidth * orgj + i] != -1) {
        data.Iz[MatrixWidth * j + i] = 3 * rindices[Iz[MatrixWidth * orgj + i]] + 2;
        data.Mz[MatrixWidth * j + i] = data.fullMz[coord0(data.n_sys, orgj, Iz[MatrixWidth * orgj + i])];
      } else {
        data.Iz[MatrixWidth * j + i] = -1;
      }
    }
  }
  reorderSubgrids(data);

  printMapping(indices, rindices);
  printIndex(data);
  printSubgrids(data);
}

//used by v8
void reorderIndizes5(DataStaRMAP& data) {
  auto indices  = getIndizes(data);
  auto rindices = getReverseIndizes(indices);

  data.M.resize(10 * data.n_sys);
  data.I.resize(10 * data.n_sys);

  data.dU.resize(3 * data.n_sys);

  auto Ix = data.Ix;
  auto Iy = data.Iy;
  auto Iz = data.Iz;

  for (unsigned int j = 0; j < data.n_sys; j++) {
    unsigned k = 0;
    int orgj   = indices[j];
    for (unsigned int i = 0; i < MatrixWidth; i++) {
      if (Ix[MatrixWidth * orgj + i] != -1) {
        data.I[10 * j + k] = 3 * rindices[Ix[MatrixWidth * orgj + i]] + 0;
        data.M[10 * j + k] = data.fullMx[coord0(data.n_sys, orgj, Ix[MatrixWidth * orgj + i])];
      } else {
        data.I[10 * j + k] = -1;
      }
      k++;

      if (Iy[MatrixWidth * orgj + i] != -1) {
        data.I[10 * j + k] = 3 * rindices[Iy[MatrixWidth * orgj + i]] + 1;
        data.M[10 * j + k] = data.fullMy[coord0(data.n_sys, orgj, Iy[MatrixWidth * orgj + i])];
      } else {
        data.I[10 * j + k] = -1;
      }
      k++;

      if (i < 2) {
        if (Iz[MatrixWidth * orgj + i] != -1) {
          data.I[10 * j + k] = 3 * rindices[Iz[MatrixWidth * orgj + i]] + 2;
          data.M[10 * j + k] = data.fullMz[coord0(data.n_sys, orgj, Iz[MatrixWidth * orgj + i])];
        } else {
          data.I[10 * j + k] = -1;
        }
        k++;
      }
    }
  }
  reorderSubgrids(data);

  printMapping(indices, rindices);
  printIndex(data);
  printSubgrids(data);
}

template <typename Container>
void printVector(const std::string& title, const Container& container) {
  std::cout << title << ": ";
  for (const auto& j : container) {
    std::cout << j << " ";
  }
  std::cout << "\n";
}

void printSubgrids(DataStaRMAP& data) {
  printVector("c111", data.c111);
  printVector("c211", data.c211);
  printVector("c121", data.c121);
  printVector("c112", data.c112);
  printVector("c221", data.c221);
  printVector("c212", data.c212);
  printVector("c122", data.c122);
  printVector("c222", data.c222);
}

void printMapping(std::vector<int> indices, std::vector<int> rindices) {
  printVector("mapping", indices);
  printVector("Rmapping", rindices);
}

void printIndex(DataStaRMAP& data) {
  std::cout << "Ix: ";
  for (unsigned int j = 0; j < data.n_sys; j++) {
    std::cout << fmt::format("[{},{},{}] ", data.Ix[MatrixWidth * j + 0], data.Ix[MatrixWidth * j + 1], data.Ix[MatrixWidth * j + 2]);
  }
  std::cout << "\nIy: ";

  for (unsigned int j = 0; j < data.n_sys; j++) {
    std::cout << fmt::format("[{},{},{}] ", data.Iy[MatrixWidth * j + 0], data.Iy[MatrixWidth * j + 1], data.Iy[MatrixWidth * j + 2]);
  }
  std::cout << "\nIz: ";

  for (unsigned int j = 0; j < data.n_sys; j++) {
    std::cout << fmt::format("[{},{},{}] ", data.Iz[MatrixWidth * j + 0], data.Iz[MatrixWidth * j + 1], data.Iz[MatrixWidth * j + 2]);
  }
  std::cout << "\n";
}

}  // namespace math
}  // namespace starmap
