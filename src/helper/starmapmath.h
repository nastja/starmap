/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "helper/datastarmap.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/simd/simd.h"
#include <array>

namespace starmap {
namespace math {

inline real_t D(real_t l, real_t m) {
  return std::sqrt((l - m) * (l - m - 1) / (2 * l + 1) / (2 * l - 1));
}

inline real_t F(real_t l, real_t m) {
  return std::sqrt((l + m) * (l + m - 1) / (2 * l + 1) / (2 * l - 1));
}

inline real_t B(real_t l, real_t m) {
  return std::sqrt((l - m) * (l + m) / (2 * l + 1) / (2 * l - 1));
}

inline unsigned int coord(unsigned int s, unsigned int i, unsigned int j) {
  return (i - 1) * s + (j - 1);
}

inline unsigned int coord0(unsigned int s, unsigned int i, unsigned int j) {
  return i * s + j;
}

void setupClosure(DataStaRMAP& data);
void setupClosureP(DataStaRMAP& data);
void makegrid(DataStaRMAP& data);
void compressMatrix(DataStaRMAP& data);
void compressMatrix2(DataStaRMAP& data);
void reorderIndizes(DataStaRMAP& data);
void reorderIndizes2(DataStaRMAP& data);
void reorderIndizes3(DataStaRMAP& data);
void reorderIndizes4(DataStaRMAP& data);
void reorderIndizes5(DataStaRMAP& data);

std::vector<int> getIndizes(DataStaRMAP& data);
std::vector<int> getReverseIndizes(std::vector<int> indices);
void reorderSubgrids(DataStaRMAP& data);
void printSubgrids(DataStaRMAP& data);
void printIndex(DataStaRMAP& data);
void printMapping(std::vector<int> indices, std::vector<int> rindices);

void addIndices(std::set<int>& c, const std::vector<int>& I, const std::set<int>& idx);
void getInterpolated(std::array<std::array<real_t, 8>, 5>& res, const unsigned long index, const nastja::field::Field<real_t, 1>& field, int start = 0);
void getInterpolated4(real_t* res, const unsigned long index, nastja::field::Field<real_t, 4>& field, int start = 0);
void useMaingrid(std::array<std::array<real_t, 8>, 5>& res, const unsigned long index, const nastja::field::Field<real_t, 1>& field, int start = 0);
void useSubgrids(std::array<std::array<real_t, 8>, 5>& res, const unsigned long index, const nastja::field::Field<real_t, 1>& field, int start = 0);

inline real_t calcSumW(unsigned int j, const DataStaRMAP& data) {
  real_t sumW = 0.0;

  for (unsigned int i = 0; i < MatrixWidth; i++) {
    sumW -= data.dxU[data.Ix[MatrixWidth * j + i]] * data.Mx[MatrixWidth * j + i];
    sumW -= data.dyU[data.Iy[MatrixWidth * j + i]] * data.My[MatrixWidth * j + i];
    sumW -= data.dzU[data.Iz[MatrixWidth * j + i]] * data.Mz[MatrixWidth * j + i];
  }

  return sumW;
}

inline real_t calcSumW_skip4(unsigned int j, const DataStaRMAP& data) {
  real_t sumW = 0.0;

  for (unsigned int i = 0; i < MatrixWidth; i++) {
    if (data.Ix[MatrixWidth * j + i] != -1) {
      sumW -= data.dxU[data.Ix[MatrixWidth * j + i]] * data.Mx[MatrixWidth * j + i];
    }
    if (data.Iy[MatrixWidth * j + i] != -1) {
      sumW -= data.dyU[data.Iy[MatrixWidth * j + i]] * data.My[MatrixWidth * j + i];
    }
    if (data.Iz[MatrixWidth * j + i] != -1) {
      sumW -= data.dzU[data.Iz[MatrixWidth * j + i]] * data.Mz[MatrixWidth * j + i];
    }
  }

  return sumW;
}

inline real_t calcSumW_skip(unsigned int j, const DataStaRMAP& data) {
  real_t sumW = 0.0;

  for (unsigned int i = 0; i < MatrixWidth; i++) {
    if (data.Ix[MatrixWidth * j + i] != -1) {
      sumW -= data.dxU[data.Ix[MatrixWidth * j + i]] * data.Mx[MatrixWidth * j + i];
    }
    if (data.Iy[MatrixWidth * j + i] != -1) {
      sumW -= data.dyU[data.Iy[MatrixWidth * j + i]] * data.My[MatrixWidth * j + i];
    }
  }

  for (unsigned int i = 0; i < MatrixWidthZ; i++) {
    if (data.Iz[MatrixWidthZ * j + i] != -1) {
      sumW -= data.dzU[data.Iz[MatrixWidthZ * j + i]] * data.Mz[MatrixWidthZ * j + i];
    }
  }

  return sumW;
}

inline real_t calcSumW_unroll(unsigned int j, const DataStaRMAP& data) {
  real_t sumW = 0.0;

  sumW -= data.dxU[data.Ix[MatrixWidth * j]] * data.Mx[MatrixWidth * j];
  sumW -= data.dyU[data.Iy[MatrixWidth * j]] * data.My[MatrixWidth * j];
  if (data.Iz[MatrixWidth * j + 0] != -1) sumW -= data.dzU[data.Iz[MatrixWidth * j]] * data.Mz[MatrixWidth * j];
  if (data.Ix[MatrixWidth * j + 1] != -1) sumW -= data.dxU[data.Ix[MatrixWidth * j + 1]] * data.Mx[MatrixWidth * j + 1];
  if (data.Iy[MatrixWidth * j + 1] != -1) sumW -= data.dyU[data.Iy[MatrixWidth * j + 1]] * data.My[MatrixWidth * j + 1];
  if (data.Iz[MatrixWidth * j + 1] != -1) sumW -= data.dzU[data.Iz[MatrixWidth * j + 1]] * data.Mz[MatrixWidth * j + 1];
  if (data.Ix[MatrixWidth * j + 2] != -1) sumW -= data.dxU[data.Ix[MatrixWidth * j + 2]] * data.Mx[MatrixWidth * j + 2];
  if (data.Iy[MatrixWidth * j + 2] != -1) sumW -= data.dyU[data.Iy[MatrixWidth * j + 2]] * data.My[MatrixWidth * j + 2];
  if (data.Ix[MatrixWidth * j + 3] != -1) sumW -= data.dxU[data.Ix[MatrixWidth * j + 3]] * data.Mx[MatrixWidth * j + 3];
  if (data.Iy[MatrixWidth * j + 3] != -1) sumW -= data.dyU[data.Iy[MatrixWidth * j + 3]] * data.My[MatrixWidth * j + 3];

  return sumW;
}

inline real_t calcSumW_unroll2(unsigned int j, const DataStaRMAP& data) {
  real_t sumW = 0.0;

  sumW -= data.dU[data.Ix[MatrixWidth * j]] * data.Mx[MatrixWidth * j];
  sumW -= data.dU[data.Iy[MatrixWidth * j]] * data.My[MatrixWidth * j];
  if (data.Iz[MatrixWidth * j + 0] != -1) sumW -= data.dU[data.Iz[MatrixWidth * j]] * data.Mz[MatrixWidth * j];
  if (data.Ix[MatrixWidth * j + 1] != -1) sumW -= data.dU[data.Ix[MatrixWidth * j + 1]] * data.Mx[MatrixWidth * j + 1];
  if (data.Iy[MatrixWidth * j + 1] != -1) sumW -= data.dU[data.Iy[MatrixWidth * j + 1]] * data.My[MatrixWidth * j + 1];
  if (data.Iz[MatrixWidth * j + 1] != -1) sumW -= data.dU[data.Iz[MatrixWidth * j + 1]] * data.Mz[MatrixWidth * j + 1];
  if (data.Ix[MatrixWidth * j + 2] != -1) sumW -= data.dU[data.Ix[MatrixWidth * j + 2]] * data.Mx[MatrixWidth * j + 2];
  if (data.Iy[MatrixWidth * j + 2] != -1) sumW -= data.dU[data.Iy[MatrixWidth * j + 2]] * data.My[MatrixWidth * j + 2];
  if (data.Ix[MatrixWidth * j + 3] != -1) sumW -= data.dU[data.Ix[MatrixWidth * j + 3]] * data.Mx[MatrixWidth * j + 3];
  if (data.Iy[MatrixWidth * j + 3] != -1) sumW -= data.dU[data.Iy[MatrixWidth * j + 3]] * data.My[MatrixWidth * j + 3];

  return sumW;
}

inline real_t calcSumW_unroll3(unsigned int j, const DataStaRMAP& data) {
  real_t sumW = 0.0;

  sumW -= data.dU[data.I[10 * j + 0]] * data.M[10 * j + 0];
  sumW -= data.dU[data.I[10 * j + 1]] * data.M[10 * j + 1];
  if (data.I[10 * j + 2] != -1) sumW -= data.dU[data.I[10 * j + 2]] * data.M[10 * j + 2];
  if (data.I[10 * j + 3] != -1) sumW -= data.dU[data.I[10 * j + 3]] * data.M[10 * j + 3];
  if (data.I[10 * j + 4] != -1) sumW -= data.dU[data.I[10 * j + 4]] * data.M[10 * j + 4];
  if (data.I[10 * j + 5] != -1) sumW -= data.dU[data.I[10 * j + 5]] * data.M[10 * j + 5];
  if (data.I[10 * j + 6] != -1) sumW -= data.dU[data.I[10 * j + 6]] * data.M[10 * j + 6];
  if (data.I[10 * j + 7] != -1) sumW -= data.dU[data.I[10 * j + 7]] * data.M[10 * j + 7];
  if (data.I[10 * j + 8] != -1) sumW -= data.dU[data.I[10 * j + 8]] * data.M[10 * j + 8];
  if (data.I[10 * j + 9] != -1) sumW -= data.dU[data.I[10 * j + 9]] * data.M[10 * j + 9];

  return sumW;
}

inline nastja::preal_t calcSumW_avx(unsigned int j, const DataStaRMAP& data) {
  auto sumW = nastja::simd::zero_pd();

  sumW -= nastja::simd::load(data.dUavx + 4 * data.Ix[MatrixWidth * j]) * nastja::simd::set_pd(data.Mx[MatrixWidth * j]);
  sumW -= nastja::simd::load(data.dUavx + 4 * data.Iy[MatrixWidth * j]) * nastja::simd::set_pd(data.My[MatrixWidth * j]);
  if (data.Iz[MatrixWidth * j + 0] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Iz[MatrixWidth * j]) * nastja::simd::set_pd(data.Mz[MatrixWidth * j]);
  if (data.Ix[MatrixWidth * j + 1] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Ix[MatrixWidth * j + 1]) * nastja::simd::set_pd(data.Mx[MatrixWidth * j + 1]);
  if (data.Iy[MatrixWidth * j + 1] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Iy[MatrixWidth * j + 1]) * nastja::simd::set_pd(data.My[MatrixWidth * j + 1]);
  if (data.Iz[MatrixWidth * j + 1] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Iz[MatrixWidth * j + 1]) * nastja::simd::set_pd(data.Mz[MatrixWidth * j + 1]);
  if (data.Ix[MatrixWidth * j + 2] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Ix[MatrixWidth * j + 2]) * nastja::simd::set_pd(data.Mx[MatrixWidth * j + 2]);
  if (data.Iy[MatrixWidth * j + 2] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Iy[MatrixWidth * j + 2]) * nastja::simd::set_pd(data.My[MatrixWidth * j + 2]);
  if (data.Ix[MatrixWidth * j + 3] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Ix[MatrixWidth * j + 3]) * nastja::simd::set_pd(data.Mx[MatrixWidth * j + 3]);
  if (data.Iy[MatrixWidth * j + 3] != -1) sumW -= nastja::simd::load(data.dUavx + 4 * data.Iy[MatrixWidth * j + 3]) * nastja::simd::set_pd(data.My[MatrixWidth * j + 3]);

  return sumW;
}

inline void calcDifferencesEven(unsigned long index, nastja::field::Field<real_t, 1>& u, DataStaRMAP& data, real_t deltax_R) {
  // case 1, tic                           % Half step with odd grids.
  // for j = c111
  //     dxU{j} = diff(U{j}(extendx{1},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{1},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendz{1}),[],3)/h(3);
  // end
  for (auto j : data.c111) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  // for j = c221      % Compute update at ... from ... .
  //     dxU{j} = diff(U{j}(extendx{2},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{2},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendy{1}),[],3)/h(3);
  // end
  for (auto j : data.c221) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  // for j = c212      % Compute update at ... from ... .
  //     dxU{j} = diff(U{j}(extendx{2},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{1},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendy{2}),[],3)/h(3);
  // end
  for (auto j : data.c212) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }

  // for j = c122      % Compute update at ... from ... .
  //     dxU{j} = diff(U{j}(extendx{1},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{2},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendy{2}),[],3)/h(3);
  // end
  for (auto j : data.c122) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }
}

inline void calcDifferencesEven_ordered(unsigned long index, nastja::field::Field<real_t, 1>& u, DataStaRMAP& data, real_t deltax_R) {
  for (int j = data.latticeEnd[0]; j < data.latticeEnd[1]; j++) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[1]; j < data.latticeEnd[2]; j++) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[2]; j < data.latticeEnd[3]; j++) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[3]; j < data.latticeEnd[4]; j++) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }
}

inline void calcDifferencesEven_ordered2(unsigned long index, nastja::field::Field<real_t, 1>& u, DataStaRMAP& data, real_t deltax_R) {
  for (int j = data.latticeEnd[0]; j < data.latticeEnd[1]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[1]; j < data.latticeEnd[2]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[2]; j < data.latticeEnd[3]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[3]; j < data.latticeEnd[4]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }
}

inline void calcDifferencesOdd(unsigned long index, nastja::field::Field<real_t, 1>& u, DataStaRMAP& data, real_t deltax_R) {
  // case 2
  //  for j = c222      % Compute update at ... from ... .
  //     dxU{j} = diff(U{j}(extendx{2},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{2},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendy{2}),[],3)/h(3);
  // end
  for (auto j : data.c222) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }
  // for j = c112      % Compute update at ... from ... .
  //     dxU{j} = diff(U{j}(extendx{1},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{1},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendy{2}),[],3)/h(3);
  // end
  for (auto j : data.c112) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }
  // for j = c121      % Compute update at ... from ... .
  //     dxU{j} = diff(U{j}(extendx{1},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{2},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendy{1}),[],3)/h(3);
  // end
  for (auto j : data.c121) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }
  // for j = c211      % Compute update at ... from ... .
  //     dxU{j} = diff(U{j}(extendx{2},:,:),[],1)/h(1);
  //     dyU{j} = diff(U{j}(:,extendy{1},:),[],2)/h(2);
  //     dzU{j} = diff(U{j}(:,:,extendy{1}),[],3)/h(3);
  // end
  for (auto j : data.c211) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }
}

inline void calcDifferencesOdd_ordered(unsigned long index, nastja::field::Field<real_t, 1>& u, DataStaRMAP& data, real_t deltax_R) {
  for (int j = data.latticeEnd[4]; j < data.latticeEnd[5]; j++) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[5]; j < data.latticeEnd[6]; j++) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[6]; j < data.latticeEnd[7]; j++) {
    data.dxU[j] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[7]; j < data.latticeEnd[8]; j++) {
    data.dxU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dyU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dzU[j] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }
}

inline void calcDifferencesOdd_ordered2(unsigned long index, nastja::field::Field<real_t, 1>& u, DataStaRMAP& data, real_t deltax_R) {
  for (int j = data.latticeEnd[4]; j < data.latticeEnd[5]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[5]; j < data.latticeEnd[6]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, nastja::stencil::Direction::F, j) - u.getCell(index, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[6]; j < data.latticeEnd[7]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, nastja::stencil::Direction::R, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, nastja::stencil::Direction::U, j) - u.getCell(index, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }

  for (int j = data.latticeEnd[7]; j < data.latticeEnd[8]; j++) {
    data.dU[3 * j + 0] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::L, j)) * deltax_R;
    data.dU[3 * j + 1] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::D, j)) * deltax_R;
    data.dU[3 * j + 2] = (u.getCell(index, j) - u.getCell(index, nastja::stencil::Direction::B, j)) * deltax_R;
  }
}

}  // namespace math
}  // namespace starmap
