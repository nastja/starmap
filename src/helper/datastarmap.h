/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/datacontainer/datacontainer.h"
#include "lib/datatypes.h"

namespace starmap {

constexpr unsigned int MatrixWidth  = 4;
constexpr unsigned int MatrixWidthZ = 2;

constexpr unsigned int grid111 = 0;
constexpr unsigned int grid112 = 1;
constexpr unsigned int grid121 = 2;
constexpr unsigned int grid122 = 3;
constexpr unsigned int grid211 = 4;
constexpr unsigned int grid212 = 5;
constexpr unsigned int grid221 = 6;
constexpr unsigned int grid222 = 7;

constexpr unsigned int data_q  = 0;
constexpr unsigned int data_sa = 1;
constexpr unsigned int data_ea = 2;
constexpr unsigned int data_st = 3;
constexpr unsigned int data_et = 4;

using namespace nastja::config::literals;

enum class Closure { P,
                     SP };

using nastja::real_t;

struct DataStaRMAP : public nastja::DataContainer {
  std::vector<real_t> M;
  std::vector<real_t> Mx;
  std::vector<real_t> My;
  std::vector<real_t> Mz;
  std::vector<real_t> fullMx;
  std::vector<real_t> fullMy;
  std::vector<real_t> fullMz;
  unsigned int n_sys;  ///< Size of the matrix

  std::vector<int> I;
  std::vector<int> Ix;
  std::vector<int> Iy;
  std::vector<int> Iz;

  std::vector<int> c11;
  std::vector<int> c12;
  std::vector<int> c21;
  std::vector<int> c22;

  std::vector<int> c111;
  std::vector<int> c121;
  std::vector<int> c211;
  std::vector<int> c221;

  std::vector<int> c112;
  std::vector<int> c122;
  std::vector<int> c212;
  std::vector<int> c222;

  std::vector<int> gtx;
  std::vector<int> gty;
  std::vector<int> gtz;
  std::vector<int> mom_order;

  std::vector<real_t> dU{};
  std::vector<real_t> dxU{};
  std::vector<real_t> dyU{};
  std::vector<real_t> dzU{};

  Closure closure;     ///< Type of closure.
  unsigned int n_mom;  ///< Order of moment approximation.

  std::array<int, 9> latticeEnd;

  real_t* dUavx;
  real_t* interpolated;

  void configure(nastja::config::Config& config) {
    /// @key{StaRMAP.closure, string, "P"}
    /// @values{"P", "SP"}
    /// Type of closure.
    auto closure_ = config.getValue<std::string>("StaRMAP.closure"_jptr, "P");
    if (closure_ == "P") {
      closure = Closure::P;
    } else if (closure_ == "SP") {
      closure = Closure::SP;
    } else {
      throw(std::invalid_argument("'StarMAP.closure must be 'P' or 'SP'."));
    }

    /// @key{StaRMAP.n_mom, uint}
    /// Order of moment approximation.
    n_mom = config.getValue<unsigned int>("StaRMAP.n_mom"_jptr);
  }

  void dump(nastja::Archive& /*ar*/) const final{};
  void undump(nastja::Archive& /*ar*/) final{};
};

}  // namespace starmap
