import glob, os, struct
import numpy as np
import json
from pathlib import Path

#calculates the error based on distance of the triangles. The triangle distance is the sum of distances between
#each point of the triangle and its nearest point of the other triangle.
#deltaY and deltaZ is the value for how far the triangles are allowed to be away in the y and z direction, respectively.
#if they are too far away the distance will not be calculated and will have no influence at final error.
#max_distance works the same but with the distance of the triangles
def diff(sim_path, test_path, deltaY = 1, deltaZ = 1, verbose = False, max_distance = 1):
  sim_normals, sim_triangles = parseSTL(sim_path)
  print(test_path)
  test_normals, test_triangles = parseSTL(test_path)
  sim_double_triangles = [two_points_equal(x, y) for x, y in zip(sim_triangles[0:-1], sim_triangles[1:])]
  sim_double_triangles.append(False)
  test_double_triangles = [two_points_equal(x, y) for x, y in zip(test_triangles[0:-1], test_triangles[1:])]
  test_double_triangles.append(False)
  normal_err = []
  triangle_err = []
  deleted = 0
  added = 0
  test_triangles_indices = [j for j in range(1, len(test_triangles)) if not test_double_triangles[j - 1]]
  test_triangles_indices.insert(0, 0)
  sim_triangles_indices = [j for j in range(1, len(sim_triangles)) if not sim_double_triangles[j - 1]]
  sim_triangles_indices.insert(0, 0)
  k = 0
  for i in sim_triangles_indices:
    k += 1
    if not test_triangles_indices:
      added += len(sim_triangles_indices) - k
      break
    #list with tuples of the form (distance, sim_index, test_index, deleted, added)
    #the indices are for the matching triangles
    distances = []
    #search matching triangles and calculate distances
    for j in test_triangles_indices:
      if abs(sim_triangles[i][2][1] - test_triangles[j][2][1]) > deltaY:
        continue
      if abs(sim_triangles[i][2][2] - test_triangles[j][2][2]) > deltaZ:
        if j > i:
          break
      if sim_double_triangles[i]:
        if test_double_triangles[j]:
          distances.append((distance([sim_triangles[i], sim_triangles[i + 1]], [test_triangles[j], test_triangles[j + 1]]), i, j, 0, 0))
        else:
          distances.append((distance([sim_triangles[i], sim_triangles[i + 1]], [test_triangles[j]]), i, j, 0, 1))
        if distances[-1][0] == 0:
          break
      else:
        if test_double_triangles[j]:
          distances.append((distance([sim_triangles[i]], [test_triangles[j], test_triangles[j + 1]]), i, j, 1, 0))
        else:
          distances.append((distance([sim_triangles[i]], [test_triangles[j]]), i, j, 0, 0))
        if distances[-1][0] == 0:
          break
      #avoid wrong matching
      if distances and distances[-1][0] > max_distance:
        distances.pop()

    if not distances:
      if sim_double_triangles[i]:
        distances.append((0, i, -1, 0, 2))
      else:
        distances.append((0, i, -1, 0, 1))
    min_distance = min(distances, key=lambda x: x[0])
    if min_distance[2] == -1:
      normal_err.append(0)
      triangle_err.append(0)
    else:
      normal_err.append(np.linalg.norm(sim_normals[min_distance[1]] - test_normals[min_distance[2]]))
      test_triangles_indices.remove(min_distance[2])
      triangle_err.append(min_distance[0])
    deleted += min_distance[3]
    added += min_distance[4]

  deleted += len(test_triangles_indices)

  result = {
    "normals": {
      "Deviations": len([x for x in normal_err if x != 0]) / len(sim_triangles),
      "MSE": np.square(normal_err).mean(),
      "MAE": np.absolute(normal_err).mean(),
      "Max": max(normal_err)
    },
    "triangles": {
      "Deviations": len([x for x in triangle_err if x != 0]) / len(sim_triangles),
      "MSE": np.square(triangle_err).mean(),
      "MAE": np.absolute(triangle_err).mean(),
      "Max": max(triangle_err),
      "Deleted": deleted,
      "Added": added
    }
  }
  with open(Path(sim_path).with_name(Path(sim_path).stem + "_stl_diff.json"), "w+") as f:
    json.dump(result, f)

  if verbose:
    print("Normals:")
    print("Deviations: " + str(len([x for x in normal_err if x != 0]) / len(sim_triangles)))
    print("MSE: " + str(np.square(normal_err).mean()))
    print("MAE: " + str(np.absolute(normal_err).mean()))
    print("Max: " + str(max(normal_err)))
    print("Triangles:")
    print("Deviations: " + str(len([x for x in triangle_err if x != 0]) / len(sim_triangles)))
    print("MSE: " + str(np.square(triangle_err).mean()))
    print("MAE: " + str(np.absolute(triangle_err).mean()))
    print("Max: " + str(max(triangle_err)))
    print("Deleted: " + str(deleted))
    print("Added: " + str(added))


def parseSTL(path):
  with open(path, 'rb') as f:
    f.read(80) #header
    numTriangles = struct.unpack('<I', f.read(4))[0]
    entries = [parseEntry(f) for _ in range(numTriangles)]
    normals, triangles = zip(*entries)
    normals = [np.array(x) for x in normals]
    triangles = [sorted(triangle, key=lambda x: (x[2], x[1])) for triangle in triangles]
    return (normals, triangles)


def parseEntry(f):
  s = f.read(50)
  normalX = struct.unpack('<f', s[:4])[0]
  normalY = struct.unpack('<f', s[4:8])[0]
  normalZ = struct.unpack('<f', s[8:12])[0]
  v1X = struct.unpack('<f', s[12:16])[0]
  v1Y = struct.unpack('<f', s[16:20])[0]
  v1Z = struct.unpack('<f', s[20:24])[0]
  v2X = struct.unpack('<f', s[24:28])[0]
  v2Y = struct.unpack('<f', s[28:32])[0]
  v2Z = struct.unpack('<f', s[32:36])[0]
  v3X = struct.unpack('<f', s[36:40])[0]
  v3Y = struct.unpack('<f', s[40:44])[0]
  v3Z = struct.unpack('<f', s[44:48])[0]
  return (np.array([normalX, normalY, normalZ]), (np.array([v1X, v1Y, v1Z]), np.array([v2X, v2Y, v2Z]), np.array([v3X, v3Y, v3Z])))


def two_points_equal(x, y):
  return np.array_equal(x[0], y[0]) and np.array_equal(x[1], y[1]) and np.array_equal(x[1], y[1])


def square_norm(x):
  #x*x is faster than x**2
  return x[0] * x[0] + x[1] * x[1] + x[2] * x[2]


#pass every np.array to the function instead of a list for performance
def triangle_distance(x0, x1, x2, y0, y1, y2):
  return square_norm(x0- y0) + square_norm(x1- y1) + square_norm(x2- y2)

def distance(x, y):
  d = [triangle_distance(t1[0], t1[1], t1[2], t2[0], t2[1], t2[2]) for t1 in x for t2 in y]
  return min(d)
