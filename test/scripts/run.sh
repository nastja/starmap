#!/bin/bash
SOLVER=$1
file=$2
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
TEST=$SCRIPTPATH/checktest.sh # relative from testdir

#relative or absolute path?
if [[ ! $file = /* ]]; then
  file=../$file
fi
if [[ ! $SOLVER = /* ]]; then
  SOLVER=../$SOLVER
fi

#create tmp-dir and copy config file and config includes to it
tmpdir=tmp_$(basename $file .json)
echo -en "\nStart test $tmpdir: "

mkdir -p $tmpdir
cd $tmpdir
if [[ $? -ne 0 ]]; then
  echo -e "\nCan't change to $tmpdir"
  exit 1
fi
cp $file .
cp -R $(dirname $file)/configinclude .

description=$(cat $file | jq  -e '."#Testing".description')
echo "$description"

numofruns=$(cat $file | jq  -e '."#Testing".flags|length')
if [[ $numofruns -ne 0 ]]; then
  numofruns=$((numofruns-1))
fi

mpiprocesses=$(cat $file | jq -e '."#Testing".mpi')
if [[ $? -ne 0 ]]; then
  mpiprocesses=0
fi

if [[ mpiprocesses -eq 0 ]]; then
  MPIRUN=""
else
  #only openMPI has "allow-run-as-root"
  if [[ $EUID -eq 0 ]]; then
    parameter="$parameter --allow-run-as-root"
  fi
  if [[ "$(mpirun -V)" = *"Open MPI"* ]]; then
     parameter="$parameter --oversubscribe"
  fi
  MPIRUN="mpirun $parameter -n $mpiprocesses"
fi

for run in `seq 0 $numofruns`; do
  echo "Run: $((run+1))/$((numofruns+1))"
  if [[ $numofruns -gt 0 ]]; then
    out="run$run"
  else
    out="run"
  fi
  execflags=$(cat $file | jq  -e '."#Testing".flags['$run']')
  if [[ $? -ne 0 ]]; then
    execflags=""
  else
    execflags="${execflags%\"}"
    execflags="${execflags#\"}"
    execflags="${execflags//\\\"/\"}"
  fi

  CMDLINE="$MPIRUN $SOLVER -c $(basename $file) $execflags"
  #run solver
  echo "Calling: $CMDLINE"
  eval $CMDLINE 1>${out}.out 2>${out}.err
  ret=$?
  if [[ ret -ne 0 ]]; then
    echo ${out}.err
    cat ${out}.err
    echo ${out}.out
    cat ${out}.out
    exit $ret
  fi
done
#pre test execution
pretestexec=$(cat $file | jq  -e '."#Testing".pretestexecution')
if [[ $? -eq 0 ]]; then
  pretestexec=$(eval echo "$pretestexec") #unescape
  echo "call pre-test execution: $pretestexec"
  eval "$pretestexec"
fi

#check result
$TEST $file
ret=$?
cd - > /dev/null

#if OK delete tmp-dir
if [[ $ret -eq 0 ]]; then
  if [[ $NASTJAKEEP -eq 1 ]]; then
    echo 'Test passed, found $NASTJAKEEP, skip deletion.'
  else
    echo "Test passed, delete test data..."
    rm -r $tmpdir
  fi
else
  cat $tmpdir/run*
fi

exit $ret
