#!/usr/bin/env python
# -*- coding: utf-8 -*-

class colors:
  GREEN = '\033[32;1m'
  RED = '\033[31;1m'
  ENDC = '\033[0m'

import glob
import sys
import os
import subprocess
import argparse
import time
import compareVTI as vti
import compareSTL as stl

VALGRIND_CMD = 'valgrind --tool=memcheck --leak-check=yes --undef-value-errors=yes '
SOLVER = './nastja '
INPUT = '../test'

def runTest(executable_path, testfile, use_valgrind):
  if args.verbose:
    output = subprocess.PIPE
  else:
    output = open(os.devnull, 'w')

  proc = subprocess.Popen(['../test/run.sh', executable_path, testfile], stdout=output, stderr=subprocess.PIPE)
  out, err = proc.communicate()
  if args.verbose:
    print(out + err)

  return proc.wait()

def runAllTests(executable_path, input_dir, use_valgrind=False):
  overallfail = 0
  print(os.path.join(input_dir, '*.json'))
  tests = sorted(glob.glob(os.path.join(input_dir, '*.json')))
  for input_file in tests:
    print('TESTING:' + input_file.ljust(50)),
    start = time.time()
    status = runTest(executable_path, input_file, use_valgrind)
    end = time.time()
    if (status == 0):
      print(colors.GREEN + '[PASS]' + colors.ENDC)
    else:
      vti.diff(input_file.rsplit('/', 1)[-1][:-5], input_dir[:-5])
      stl.diff(input_file.rsplit('/', 1)[-1][:-5], input_dir[:-5])
      print(colors.RED + '[FAIL]' + colors.ENDC)
      overallfail = 1
    print("%.2f s" % (end-start))

  return overallfail

parser = argparse.ArgumentParser()
parser.add_argument("-V", "--valgrind", action="store_true", help="run all the tests using valgrind to detect memory leaks")
parser.add_argument("-v", "--verbose", action="store_true", help="verbose output of jobs")
args = parser.parse_args()

status = runAllTests(SOLVER, INPUT, use_valgrind=args.valgrind)
if status:
  sys.exit(status)
