#!/usr/bin/env python3
from pathlib import Path
import glob
import cmd
import configparser
import distutils
import os
import sys
import subprocess
import shutil
import json
import fnmatch
import signal
from run import run as runtest
from printdiff import print_diffs

class colors:
  GREEN = '\033[32;1m'
  RED = '\033[31;1m'
  ENDC = '\033[0m'

class TestShell(cmd.Cmd):
  intro = "Welcome to the NAStJA test shell. Type 'help' or '?'' for a full command list.\n"
  prompt = '(nastja test) '
  executable_name = "nastja"
  testcase_folder = "test"

  nastja_srcdir = Path(os.path.dirname(os.path.realpath(__file__))).parent.parent
  nastja_exe = Path(os.getcwd() + "/" + executable_name)

  if not os.path.isfile(nastja_exe):
    print("The nastja executable is not found. Please run the NAStJA testing shell from the build folder, and check if it is built.")
    exit(1)

  __hidden_methods = ('do_EOF', 'do_q', "do_set_conf_and_data_path")

  def get_names(self):
    return [n for n in dir(self.__class__) if n not in self.__hidden_methods]

  # get all test files
  tests = subprocess.run([str(nastja_srcdir.joinpath(testcase_folder, 'scripts/gettests.sh'))], stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
  tests = list(filter(None, tests))
  tests = [Path(test).stem for test in tests]

  successfull_tests = [False] * len(tests)

  def __init__(self, config):
    self.config = config
    parser = configparser.ConfigParser()
    parser.read(self.config)
    if "DEFAULT" in parser:
      if "DataPath" in parser["DEFAULT"]:
        self.test_data_path = parser["DEFAULT"]["DataPath"]
    super(TestShell, self).__init__()

  def complete_tests(self, text, line, start_index, end_index):
    if text:
      return [
        test for test in self.tests
        if test.startswith(text)
      ]
    else:
      return self.tests


  def do_list(self, arg):
    "List all tests."
    for test, successfull in zip(self.tests, self.successfull_tests):
      color_start = ""
      color_end = ""
      appendix = ""
      if test in [Path(test).stem[4:] for test in glob.glob("test/tmp_*")]:
        appendix = " [Failed]"
        color_start = colors.RED
        color_end   = colors.ENDC
      elif successfull:
        appendix = " [Passed]"
        color_start = colors.GREEN
        color_end   = colors.ENDC
      print(color_start + Path(test).stem + appendix + color_end)



  def do_run(self, arg, skip_test=False, sha="", remove_data=True, skip_md5=False):
    "Run a specified test, i.e., run basic_01"
    test_found = False
    for test, i in zip(self.tests, range(len(self.successfull_tests))):
      if fnmatch.fnmatch(test, arg):
        os.chdir("./test")
        res = runtest(str(self.nastja_exe), self.nastja_srcdir.joinpath(self.testcase_folder, test + ".json"), skip_test, sha, skip_md5)
        if res == 0:
          path = "./tmp_" + test
          if os.path.isdir(path) and remove_data:
            shutil.rmtree(path)
          self.successfull_tests[i] = True
        else:
          self.successfull_tests[i] = False
        os.chdir("../")
        test_found = True

    if not test_found:
      print("Test '" + arg + "' not found.")


  def complete_run(self, text, line, start_index, end_index):
    return self.complete_tests(text, line, start_index, end_index)


  def do_runall(self, arg):
    "Run all tests."
    for test in self.tests:
      self.do_run(test)


  def do_show(self, arg):
    "Print all diff files for the test, i.e., show basic_01"
    args = arg.split(" ")
    verbose = False
    failed_only = False

    for arg in args:
      if arg == "verbose":
        verbose = True
      elif arg == "failed-only":
        failed_only = True

    if len(args) <= int(failed_only) + int(verbose):
        print("No test name provided")
        return

    test = args[int(failed_only) + int(verbose)]

    if not os.path.isdir("./test/tmp_" + test):
      self.do_run(test, remove_data=False, skip_md5=True)
    if os.path.isdir("./test/tmp_" + test):
      print_diffs(self.nastja_srcdir.joinpath(self.testcase_folder, test + ".json"), "./test/tmp_" + test, verbose, failed_only)


  def complete_show(self, text, line, start_index, end_index):
    return self.complete_tests(text, line, start_index, end_index)


  def do_accept(self, arg):
    "Replace test data with the new data for specified test, i.e., accept basic_01"
    for test in self.tests:
      if fnmatch.fnmatch(test, arg):
        if not os.path.isdir("./test/tmp_" + test):
          self.do_run(test, skip_test=True)
        if os.path.isdir("./test/tmp_" + test):
          subprocess.run("python3 " + str(self.nastja_srcdir) + "/test/scripts/replaceTestData.py " + "./test/tmp_" + test +
                          " " + self.test_data_path + "/testdata/" + test +
                          " " + str(self.nastja_srcdir.joinpath(self.testcase_folder)), shell=True)


  def complete_accept(self, text, line, start_index, end_index):
    return self.complete_tests(text, line, start_index, end_index)


  def do_compare(self, arg):
    "Compares data after test execution with the test data of the specified branch , i.e., compare test branch"
    args = arg.split(" ")
    test = args[0]

    sha_path = Path("test/hashes/" + Path(test).stem + ".sha")

    if len(args) > 1:
      sha = subprocess.run("git show " + args[1] + ":" + str(sha_path), cwd=self.nastja_srcdir, stdout=subprocess.PIPE, shell=True).stdout

    sha = sha.decode('UTF-8')
    print(sha)
    self.do_run(test, sha=sha, remove_data=False, skip_md5=True)

    self.do_show(test)


  def complete_compare(self, text, line, start_index, end_index):
    return self.complete_tests(text, line, start_index, end_index)

  def do_set_data_path(self, arg):
    "Sets the path of the test data"
    config = configparser.ConfigParser()
    config_path = Path.home().joinpath(".config/nastja/test.conf")

    if not os.path.isdir(config_path.parent):
      os.makedirs(config_path.parent)

    if not os.path.isfile(config_path):
      data_path = arg
      if not Path(data_path).is_dir():
        os.makedirs(data_path)
      config['DEFAULT'] = {"DataPath": data_path}
      with open(config_path, 'w+') as configfile:
        config.write(configfile)

    self.test_data_path = arg


  def do_set_conf_and_data_path(self, arg):
    "Sets the path of the test data"
    args = arg.split(" ")
    config = configparser.ConfigParser()
    config_path = Path(args[0]).joinpath("test.conf")

    if not os.path.isdir(config_path.parent):
      os.makedirs(config_path.parent)

    if not os.path.isfile(config_path):
      data_path = args[1]
      if not Path(data_path).is_dir():
        os.makedirs(data_path)
      config['DEFAULT'] = {"DataPath": data_path}
      with open(config_path, 'w+') as configfile:
        config.write(configfile)

    self.test_data_path = args[1]


  def do_clean(self, arg):
    "Deletes all test data."
    for test in self.tests:
      path = "./test/tmp_" + test
      if os.path.isdir(path):
        shutil.rmtree(path)


  def do_exit(self, arg):
    "Close the shell."
    print()
    return True


  def do_q(self, arg):
    return self.do_exit(arg)


  def do_EOF(self, arg):
    return self.do_exit(arg)

  def emptyline(self):
    pass


args = sys.argv

if len(args) > 1 and args[1] == "set_conf_and_data_path":
  TestShell(args[2]).onecmd(" ".join(args[1:]))
else:
  config = configparser.ConfigParser()
  config_path = Path.home().joinpath(".config/nastja/test.conf")

  if not os.path.isdir(config_path.parent):
    os.makedirs(config_path.parent)

  if not os.path.isfile(config_path):
    print("~/.config/nastja/test.conf was not found. A new conf file will be created.")
    print("Where do you want to save the test data?")
    data_path = str(Path.home()) + input(Path.home())
    if not Path(data_path).is_dir():
      os.makedirs(data_path)
    config['DEFAULT'] = {"DataPath": data_path}
    with open(config_path, 'w+') as configfile:
      config.write(configfile)


  shell = TestShell(config_path)
  if len(args) > 1:
    shell.onecmd(" ".join(args[1:]))
  else:
      s = signal.signal(signal.SIGINT, signal.SIG_IGN)
      shell.cmdloop()
      signal.signal(signal.SIGINT, s)
