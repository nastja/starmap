# Contributing to NAStJA

## Git branches

Our primary branch is `master`. Only fast-forward merge requests are allowed. Also, all test must be successful `make test`.

Please ensure that you are basing your work on top of the `master` branch. For a linear history, use rebase instead of merging. It is fine if you rebase your branch and you are the only developer on it. It is allowed to use force push, `git push --force-with-lease`.
In case of a shared branch use an appendix number for each rebasing.

You can find many documents about linear git history in the web, e.g., [http://www.bitsnbites.eu/a-tidy-linear-git-history/](http://www.bitsnbites.eu/a-tidy-linear-git-history/).

For default action
```
git config --global pull.rebase true
```

Please follow the following naming scheme when pushing a topic branch:

- `feature/foo-bar` for new features in the core of NAStJA
- `cells/feature/foo-bar` for new features in the scientific engine of NAStJA, here `cells` stands for the Cellular Potts Model
- `fix/foo-bar` for bug fixes
- `tests/foo-bar` for change only concerns the test suite
- `clean/foo-bar` for clean the code style
- `docs/foo-bar` for updating the documentation

These topic branches should be small and should early merge into the `master`. Complicated topics that are not approvable for the master can be collected in develop branches as `cells/develop` for the Cellular Potts Model.

If you have to merge manually, use
```
git merge --no-ff my-topic-branch
```
to preserve the history of a feature.

## Style guide
Use the provided clang-format style. To prevent whitespace changes.
