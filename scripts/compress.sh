#!/bin/bash
#compress data needed to trace coverage in testing
find . -name "*.gcno" > files
echo "nastja_starmap" >> files
echo "unittest/ut_starmap" >> files
echo "CTestTestfile.cmake" >> files
echo "unittest/CTestTestfile.cmake" >> files
echo "test/CTestTestfile.cmake" >> files
tar czf artifacts.tar.gz --files-from files
ls -sh artifacts.tar.gz
